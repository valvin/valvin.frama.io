+++
date = "2015-06-20T14:02:37+02:00"
title = "À propos"
hidden = true
menu = "main"
+++

Bienvenue sur mon Blog. Je suis Valvin, passioné d'informatique, je parle ici d'un peu de [Libre](/tags/opensource), de décentralisation d'Internet, de respect de la [vie privée](/tags/vie-priv%C3%A9e/) ou d'auto-hébergement et des sujets techniques qui l'accompagne. 
De temps en temps, je parle aussi du projet Libre pour lequel j'ai la chance de contribuer, la bande-dessinées de David Revoy, [Pepper&Carrot](https://peppercarrot.com).

Ce site est généré avec [Hugo](http://gohugo.io/) grâce à l'instance Gitlab de Framasoft et sa CI/CD, [Framagit](https://framagit.org), et repose sur le thème [Ananké](https://github.com/budparr/gohugo-theme-ananke/) que j'ai adapté notamment pour intégrer Isso.

Vous pouvez suivre ce blog avec son [flux RSS](/index.xml) ou en me suivant sur le Fediverse, pour ma part je me trouve sur ma propre instance [Friendica](https://friendi.ca) avec le compte [@valvin@friendica.valvin.fr](https://friendica.valvin.fr/profile/valvin). 
