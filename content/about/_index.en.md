+++
date = "2015-06-20T14:02:37+02:00"
title = "À propos"
hidden = true
menu = "main"
+++

Welcome on my Blog. I'm Valvin a (french) computing enthousiast. I talk here about [Free software](/tags/opensource), Internet decentralization and federation, [privacy](/tags/vie-priv%C3%A9e/) and self-hosting and technical thing which are around. Translation of this blog is incomplete, french version is more complete.

Time to time, I talk about the Opensource project on which I'm a lucky contributor, David Revoy's webcomic [Pepper&Carrot](https://peppercarrot.com).

This website is genreated by [Hugo](http://gohugo.io) using Framasoft Gitlab CI/CD [Framagit](https://framagit.org). It uses [Ananké](https://github.com/budparr/gohugo-theme-ananke/) theme that I've modified to integrate comment with Isso.

You can follow this blog using its [RSS feed](/index.xml) or following me on the Fediverse. On my side I'm using my own [Friendica](https://friendi.ca) instance with the account [@valvin@friendica.valvin.fr](https://friendica.valvin.fr/profile/valvin)
