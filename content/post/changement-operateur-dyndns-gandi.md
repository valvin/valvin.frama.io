---
title: "Comment surveiller sa connexion ADSL chez Orange?"
date: 2019-10-06T21:16:25+02:00
tags: ["dns","hébergement", "adsl"]
draft: false
---

Récemment j'ai fait le choix de changer d'opérateur **ADSL** (oui, je n'ai pas encore la fibre...). Depuis de nombreuses années j'étais chez **Free** mais malheureusement depuis 3 ans, ils ont été dans l'incapacité à corriger la problématique de désynchronisation qui apparaissait notamment lors de forte chaleur. J'ai ouvert de nombreux tickets qui ont fait intervenir 3 techniciens (un par an) et qui ont **tous signalé un défaut sur la ligne**. À chaque fois, la procédure était la même, l'ouverture du ticket délcenchait l'intervention du technicien, le technicien constatait le défaut, ma ligne était mise sous surveillance, l'**incident était clos**. Lors du passage du dernier technicien, j'ai essayé d'avoir plus d'information. Celui-ci était assez formel, il fallait que Free déclenche une **intervention chez France Télécom**. Sauf que ces actions été déclenchées par le *back-office* et qu'il pouvait simplement stipuler quelques éléments dans ses notes mais pas plus. De ce fait, lors de la dernière clôture de ticket, j'ai décidé de résilier. J'ai choisi d'aller vers **Orange** me disant que je n'aurais pas cet effet "*parapluie*" : "*C'est pas moi, c'est lui!*". 

Avant ça, il fallait que je trouve une solution pour ma messagerie que j'héberge sur mon NAS Synology. Chez Free, j'avais une adresse **IP fixe** sur laquelle je pouvais mettre un enregistrement *reverse*. Ce n'est plus le cas avec Orange, l**'IP est dynamique** et je n'imagine même pas pour l'enregistrement *reverse*. J'ai opté pour la solution de facilité, j'ai migré ma messagerie chez **Tutanota** [^1] en version *Premium*. Ce n'est pas *iso* mais ça fait le travail. Après quelques minutes de reconfiguration DNS, le tour était joué, je pouvais résilier sereinement.

Après un peu plus d'un mois chez Orange, je n'ai pas constaté de problème particulier et d'après le peu de données disponibles sur la livebox c'est cohérent. Mais bon, j'aimerais quand même avoir un avis plus objectif et  **vérifier depuis l'extérieur que ça *ping* bien**.

Alors comment faire quand on a pas d'adresse IP fixe? La solution existe depuis fort longtemps que l'on nomme souvent ***DynDns***. La livebox permet de configurer ce type de service mais en lisant les quelques avis sur Internet, je me suis dit qu'on pouvait certainement faire autrement.

Mon DNS est chez **Gandi** et celui-ci est modifiable via API. Et effectivement, il ne m'a pas fallu très longtemps pour trouver un projet qui allait répondre à mon besoin : **Gandi Live Dns** [^2]

Pour fonctionner, il a besoin d'un nom de domaine hébergé chez *Gandi*, un ou plusieurs sous-domaine à mettre à jour, la clé API qui va bien pour modifier les données et pour terminer un service qui renvoie l'adresse IP appelante. Plutôt que d'utiliser un service existant, j'ai déployé rapidement un container de l'image ***echo-ip*** [^3] sur mon serveur hébergé chez Scaleway avec un playbook Ansible.

Voici à quoi ressemble le playbook qui s'appuie sur le module *docker_container* [^4] d'Ansible : 

```yaml
- hosts: all
  tasks:
    - name: container docker echoip
      docker_container:
        name: echoip
        image: mpolden/echoip
        state: started
        restart: yes
        pull: yes
        recreate: yes
        command: "/opt/echoip/echoip -H X-Real-IP"
        ports:
          - "127.0.0.1:8089:8080"
      tags: echoip
```

Comme le container se trouve derrière Nginx qui expose le service avec un certificat Let's Encrypt, il fallait rajouter l'option qui permet d'indiquer où se trouve l'adresse IP réel de l'appelant, dans mon cas `X-Real-IP`

Le service est disponible (pour l'instant) [ici](https://vps2.valvin.fr)

Il ne restait plus qu'à configurer *Gandi Live Dns*, activer une tâche dans le cron et *hop!* j'ai un nom DNS avec mon adresse IP tout en évitant les sites qui semblent plus ou moins douteux.

Après avoir configurer le firewall de la livebox pour autoriser le ping, j'ai pu (enfin) rajouter une sonde sur ***Uptime Robot*** [^5] et je serais notifier quand ça ne fonctionnera pas. Espérons ne pas l'être.

[^1]: https://www.tutanota.com/fr/
[^2]: https://github.com/cavebeat/gandi-live-dns
[^3]: https://github.com/mpolden/echoip
[^4]: https://docs.ansible.com/ansible/latest/modules/docker_container_module.html
[^5]: https://uptimerobot.com/



