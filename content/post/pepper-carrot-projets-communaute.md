+++
author = "ValVin"
categories = ["peppercarrot", "opensource"]
date = 2017-08-01T18:38:30Z
description = "En plus de la bande-dessinée, des projets réalisés par la communauté Pepper&Carrot se réalise. En voici un liste non exhaustivie."
draft = false
featured_image = "/content/images/2017/08/2015-09-15_witches-of-chaosah-family_by-David-Revoy.jpg"
slug = "pepper-carrot-projets-communaute"
tags = ["peppercarrot", "opensource"]
title = "Les petits projets de la communauté Pepper&Carrot"

+++

Je suis un fan inconditionnel de la bande-dessinée **Pepper&Carrot** de David Revoy. Je pense que ceux qui ne le savent pas ne me connaissent pas vraiment :)

J'admire la qualité du travail réalisé, notamment le graphisme qui est tout simplement exceptionnel ! Mais j'admire également le projet qui est bien plus qu'un Webcomic ou une BD traditionnelle. Pour ne citer que quelques points : 

* le travail est réalisé entièrement avec des logiciels libres notamment [Krita](https://krita.org/fr/) et [Inkscape](https://inkscape.org/en/) et sous licence libre ([CC-BY](https://creativecommons.org/licenses/by/4.0/))
* l'auteur partage toutes les étapes de son travail avec la communauté
* l'auteur permet à la communauté de participer à ce magnifique projet
* ... 

L'objet de ce billet n'est pas de parler directement de la bande-dessinée mais des petits projets de la communauté Pepper&Carrot à laquelle j'ai la chance de participer.

La [communauté Pepper&Carrot](https://www.peppercarrot.com/fr/static4/community) se retrouve sur le canal IRC [#pepper&carrot](http://webchat.freenode.net/?channels=%23pepper%26carrot) sur freenode ou au travers des différents bridge comme [Telegram](https://telegram.me/joinchat/BLVsYz_DIz-S-TJZB9XW7A) ou encore [Framateam](https://framateam.org/peppercarrot/). En tout ~~objectivité~~ subjectivité, j'ai une préférence pour le dernier :) Petit clin d'oeil au développeur de [Matterbridge](https://github.com/42wim/matterbridge) qui est d'une réactivité phénomènale dès que j'ai un petit souci.

Et justement l'objet de ce post et de parler de ces différents petits projets qui fleurissent ici et là. Beaucoup sont en cours de réalisation, d'autres on déjà bien avancé, dont je vais essayer de faire une liste que j'essaierais de mettre à jour au fil de l'eau car j'en aurais forcément oublié :)

### Pepper&Carrot "Mini" par nartance

En plus de ces nombreux [Fan Art](http://nartance.deviantart.com/gallery/62154968/Pepper-and-Carrot-fanarts), **nartance** propose depuis peu une série d'épisodes de courtes aventures avec Pepper, Shichimi, Coriandre et Safran mais en version "Mini"

Ci-dessous, le premier épisode, si vous souhaitez retrouver les suivantes, c'est par [ici](http://nartance.deviantart.com/) (version anglaise disponible pour chaque épisode)   
![](http://pre08.deviantart.net/bb98/th/pre/i/2017/121/b/d/_pepper_et_carrot_mini___1___le_feu_de_camp_by_nartance-db7r00n.png)

### Un lecteur d'épisodes pour Android par imsesaok

Un premier lecteur d'épisode a été développé par la société InteractiveBox mais celui-ci n'est malheureusement pas (encore) open-source. Celui que propose **imsesaok** est en cours de développement mais me semble prometteur. Pour le dépôt github c'est par [ici](https://github.com/qtwyeuritoiy/peppercarrot-reader). Il est développé dans le langage [Kotlin](https://kotlinlang.org/) qui se veut être un remplaçant en douceur de Java pour Android, mais pas que (si je dis une bêtise soyez indulgent).

Dans les fonctionnalités, actuellement vous pouvez lire les épisodes en anglais uniquement mais très prochainement grâce à la génération de [meta-data](https://github.com/qtwyeuritoiy/peppercarrot-metadata), il sera possible de consulter les épisodes dans l'ensemble des langues que la communauté a traduit.

L'application est actuellement disponible sur le [Play-Store](https://play.google.com/store/apps/details?id=nightlock.peppercarrot)
![](/content/images/2017/08/peppercarrot-reader.png)

### Le jeu vidéo par eirexe

Une première version 2d était en cours de développement par un petit groupe dont **eirexe** mais qui récemment a été arrêté. Cependant c'est pour repartir sur un nouvelle base et si j'ai bien compris, cette fois-ci le jeu sera réalisé avec des modèles 3d.
Le code source de la première version est disponible [ici](https://github.com/PepperCarrotGame/peppercarrotgame_source). 

![](/content/images/2017/08/Screenshot_2017-08-01_22-00-08.png)

### Le thème Pepper&Carrot dans Consistency de fsvieira 

**Consistency** est un jeu de logique de type [puzzle zebra](https://en.wikipedia.org/wiki/Zebra_Puzzle). Si j'ai compris, il s'agit de déterminer la case qui correspond en fonction des différents indices donnés. Récemment **fsvieira** travaille un thème spécifique pour Pepper&Carrot dont on peut voir l'évolution [ici](https://fsvieira.github.io/consistency/#!/pepperandcarrot).

![](https://fsvieira.github.io/consistency/site/images/pepperandcarrot/pepper-and-carrot-4x4.png)

### Les traductions

Une des forces de Pepper&Carrot est la disponibilité des différents épisodes dans de nombreuses langues dont je ne citerais pas tous les contributeurs mais dont on peut avoir un bel aperçu [ici](https://www.peppercarrot.com/en/static6/sources&page=translation).

Actuellement sur 22 épisodes, **663 traductions** ont été réalisées pour **38 langues** différentes. J'ai connu certaines langues dont le [Lojban](https://fr.wikipedia.org/wiki/Lojban) grâce à la traduction dans Pepper&Carrot.

### Le Wiki

On y retrouve tous les personnages, les lieux et créatures d'Hereva mais aussi une information très riche de l'ensemble de l'univers ou les aventures de Pepper&Carrot se déroulent. Notamment grâce **cmaloney** qui l'alimente régulièrement.
Le wiki c'est par [ici](https://www.peppercarrot.com/static8/wiki) pour la lecture et le dépot github est [ici](https://github.com/Deevad/peppercarrot/wiki)

### Tout le reste

Je dois oublier d'autres petits projets dont je ne suis pas forcément au courant. Si j'en ai oublié laissez-moi un petit commentaire et je complèterais :)










