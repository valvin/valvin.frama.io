+++
author = "ValVin"
categories = ["dns", "smtp"]
date = 2015-10-12T09:54:00Z
description = "Ce premier article présente le DNS et ses différents types d'entrées et notamment le type MX pour les messageries"
draft = false
featured_image = "/content/images/2015/10/mailbox-595854_1280.jpg"
slug = "messagerie-dns-mx"
tags = ["dns", "smtp"]
title = "Messagerie - DNS - MX"

+++

Il y a quelques temps, l'opération menée par [Framasoft](http://www.framasoft.org), [Dégooglisons Internet](https://degooglisons-internet.org/), m'a ouvert les yeux sur la nécessité de ne plus fournir toute ma vie numérique aux géants de l'Internet, notamment le GAFAM (Google, Apple, Facebook, Amazon, Microsoft).

Après avoir rapatrier mes fichiers photos sur mon NAS Synology, il m'est vite venu l'envie de ne plus utiliser ma messagerie Gmail que j'ai depuis 2003. Synology a l'avantage de faciliter un peu les choses et m'a éviter de trop perdre de temps sur Postfix, Roundcube et cie.

Par contre, il est nécessaire de paramétrer son nom de domaine pour que tout fonctionne correctement.

Ce premier article, décrit rapidement les différents types d'*enregistrements DNS* qui nous seront nécessaires **A**, **AAAA**, **CNAME**, **TXT** et bien entendu le type **MX**.

Dans les articles suivants, nous regarderons les enregistrements DNS **SPF**, **DKIM** et **DMARC** qui permettent de "sécuriser" sa messagerie notamment contre le SPAM.

![](/content/images/2015/10/mail-364171_640-resized.png)

###Les enregistrements DNS pour sa messagerie
En guise d'introduction, voici une petite vidéo de l'AFNIC qui explique bien le principe :

<iframe width="420" height="315" src="https://www.youtube.com/embed/dcIrB8qRCbA" frameborder="0" allowfullscreen></iframe>

ou encore cet article Wikipédia : [Domain Name System](https://fr.wikipedia.org/wiki/Domain_Name_System)

####Les enregistrements DNS - A - AAAA - CNAME - TXT - MX

L'enregistrement de type **A** est le plus basique des enregistrements DNS. Il associe un nom à une adresse IPv4 qui est LA raison d'être des serveurs DNS. L'enregistrement **AAAA** est équivalent mais permet d'associer un nom à une adresse IPv6.
```
www.fdn.org => (A) 80.67.169.18
www.fdn.org => (AAAA) 2001:910:800::18
```
L'enregistrement de type **CNAME** est également souvent utilisé et permet de rediriger un enregistrement vers un autre nom. Il arrive régulièrement qu'un enregistre CNAME est créer afin de donner un nom "user-friendly" vers le nom du serveur qui peut être plus technique (même si ce n'est pas forcément l'unique raison).

```
www.framasoft.org =>(CNAME) framasoft.org => (A) 144.76.131.212
```

```
www.framasoft.org	canonical name = framasoft.org.
Name:	framasoft.org
Address: 144.76.131.212
```

L'enregistrement de type **TXT** n'est pas le plus utilisé lors de notre utilisation d'Internet quotidienne. Il s'agit d'un enregistrement technique qui permet de retourner une valeur pour une clé donnée. Ce type d'enregistrement est notamment utilisé pour l'authentification du domaine et les enregistrements SPF,DKIM et DMARC.
```
framasoft.org	text = "v=spf1 mx a ?all"
```

Chaque enregistrement dispose d'un **TTL** qui correspond à la durée qu'un serveur DNS peut conserver cet enregistrement dans son cache. Cela correspond au temps maximum de prise en compte d'une modification de cet enregistrement. Cette valeur est exprimée en secondes. La valeur 10800 (3h) semble être régulièrement utilisée.
  
Quand dans votre navigateur vous tapez `https://blog.valvin.fr`, votre système éxectue la requête DNS suivante :
```
nslookup blog.valvin.fr
```

Dans cet exemple, `blog.valvin.fr` est un enregistrement CNAME vers `vps1.valvin.fr` qui lui même un enregistrement A qui pointe vers `149.202.44.57`

```
blog.valvin.fr	canonical name = vps1.valvin.fr.
Name:	vps1.valvin.fr
Address: 149.202.44.57
```
Votre navigateur se connectera donc sur le port 443 de l'adresse IP 149.202.44.57.

Si votre serveur de messagerie s'appelle `mail.domain.tld`, il faut tout d'abord créer l'enregistrement de type A (et AAAA si IPv6). 
L'enregistrement de type A correspondra donc à ça :
```
mail 10800 IN A 100.101.102.103
```
ou passé par CNAME dont la destination disposera d'un registrement de type A :
```
mail 10800 IN CNAME server.domain.tld.
```
Dans les deux cas, *10800* correspond au TTL de l'enregistrement.

**NB** : je note les enregistrements DNS au format texte, mais normalement votre hébergeur propose une interface graphique qui permet de créer plus faciliment ces enregistrements.

####L'enregistrement MX
L'enregistrement MX est celui qui indique aux serveurs SMTP quels sont les serveurs SMTP pour le domaine en question.
Par exemple, lors que j'envoie un message à prenom.nom@free.fr, ma messagerie effectue la requête DNS suivante :
```
nslookup -type=MX free.fr
```
Cette requête retourne plus enregistrements avec des priorités différentes :
```
Non-authoritative answer:
free.fr	mail exchanger = 10 mx1.free.fr.
free.fr	mail exchanger = 20 mx2.free.fr.
```
Il y a deux serveurs SMTP disponible `mx1.free.fr` et `mx2.free.fr`. Ce qui apparait en tant que "mail exchanger" avec la commande nslookup correspond en réalité à la priorité du serveur. Celui qui a la priorité la plus faible est prioritaire.

Donc ma messagerie communiquera avec le serveur SMTP mx1.free.fr ; si elle n'y arrive pas, elle tentera sa chance sur mx2.free.fr.

Il faut donc maintenant créer cet enregistrement DNS MX qui correspondra au serveur SMTP de votre messagerie pour lequel vous avez créé l'enregistrement A ou CNAME.

Cela ressemble à ceci :
```
@ 10800 IN MX 10 mail.domain.tld.
```
*@* : correspond au domaine racine sur lequel on travail, `domain.tld` dans notre exemple

*10800* : Le TTL de notre enregistrement
*MX* : correspond au type d'enregistrement "mail exchanger"

*10* : est la priorité, 10 correspond souvent au serveur prioritaire, mais ce n'est pas forcément LA  norme.

*mail.domain.tld.* : correspond à la valeur de cet enregistrement MX et qui est donc notre serveur SMTP  

Si on dispose de plusieurs serveurs SMTP, il suffit de créer un second enregistrement MX avec une priorité différente :
```
@ 10800 IN MX 20 mail2.domain.tld.
```

Cette configuration est suffisante pour faire fonctionner le protocole STMP. Cependant, beaucoup de serveur SMTP rejetteront votre mail ou le considéreront comme SPAM. C'est la raison pour laquelle dans les prochains articles nous allons voir comment faire les enregistrements SPF, DKIM et DMARC.