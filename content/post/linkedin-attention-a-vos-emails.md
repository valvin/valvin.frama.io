+++
author = "ValVin"
categories = ["vie-privée"]
date = 2017-03-01T19:03:02Z
description = ""
draft = false
featured_image = "/content/images/2017/03/envelope-1829509_1280.jpg"
slug = "linkedin-attention-a-vos-emails"
tags = ["vie-privée"]
title = "LinkedIn - attention à vos emails !"

+++

Je dispose d'un compte **LinkedIn** qui comme tout réseau social a pour premier objectif de collecter des données pour mieux vendre votre profil.

Même si c'est *mal* d'un point de vue vie-privée, je le conserve car cela reste une vitrine professionnelle et qui sait cela peut toujours servir.

Récemment, je suis aller faire un tour sur le site pour regarder ma quarantaine d'invitation en attente pour accepter les personnes que je connais réellement.

Bien entendu, ma curiosité m'a orienté vers la rubrique *Les connaissez-vous?* et je voies des personnes que je connais mais qui n'ont pas de compte LinkedIn (*Inviter* au lieu *Se Connecter*). Et donc je me demande comment ont-il récupérer cette information? 

Il s'avère qu'il s'agit d'une erreur de jeunesse, à l'époque ou j'utilisais tous le services Google parce que c'était bien. J'avais installé l'application LinkedIn Android qui par défaut demande l'accès au contact du téléphone qui, à l'époque bien entendu, étaient synchroniser à mon compte Gmail. L'application a donc récupéré l'ensemble de ces informations pour ~~améliorer l'expérience utilisateur~~ rentabiliser sa base de données et essayer d'étendre son réseau.

Voici comment obtenir ces données que j'ai découvert en réglant mes options de confidentialité. Il suffit d'utiliser la fonctionnalité "Archives de vos données" 

![](/content/images/2017/03/LinkedIn_archives_de_vos_donnees.png)

Si on demande les données rapides, on obtient une archive contenant plusieurs fichiers CSV. J'avoue que j'ai été surpris des données que j'ai trouvées même si après coup, il n'y a rien d'exceptionnelle. (j'ai fait la demande des données complète, il y a peut être d'autres choses intéressantes)

Je vous conseille de regarder le fichier `Contacts.csv` qui correspond aux données récupérées soit par l'application mobile ou import manuel. Les données sont minimales : `Prénom, Nom, Mail`. Dans mon cas, j'ai pas mal d'adresse mail sans prénom ni nom, ce qui est représentatif des collectes de mails Gmail. (tout au moins de l'époque)

Ensuite, il y a le fichier `Connections.csv`, il s'agit de l'ensemble de vos relations. On retrouve les informations suivantes `Prénom,Nom,mail,Société,Mission,Type relation` 
Ce qui m'a surpris c'est la possibilité de récupérer les adresses mails de l'ensemble de ses contacts alors que je pensais ne pas avoir l'information sur linkedin ... mais en fait je me trompais, l'adresse mail est bien affiché sur chaque profil en tout cas lorsque l'on est contact de 1er niveau.

Pour ceux que ça intéresse, voici mon paramétrage confidentialité qui par défaut est ... très orienter exploitation des données : 

![](/content/images/2017/03/LinkedIn_confidentialit--1.png)