---
title: "L'affaire Huawei avec Les États-Unis"
date: 2019-06-02T17:21:53+02:00
tags: ['android','opensource','huawei','états-unis','chine']
draft: false
---

L'affaire Huawei que l'on entend beaucoup parlé c'est dernier temps m'interpelle, peut être naïvement, sur les possibilités dont dispose un gouvernement comme les Etats-Unis. Je ne suis peut être pas assez documenté sur le sujet et j'ai peut être un bias dans mon jugement. Mais globalement voici ce que je comprends.

Tout d'abord, l'affaire semble débuter avec l'interdiction d'utiliser du matériel Huawei aux Etats-Unis pour des raisons d'espionnage. Il me semble que d'autres pays ont également suivis. J'avoue que sur ce point, je n'ai pas été tant surpris et cela me parait normal que l'infrastructure de télécommunications que l'on mette dans un pays soit sûre et de confiance. Vu le côté stratégique du sujet, que l'on utilise des constructeurs nationaux ne me choquerait même pas. 

Les Etats-Unis sont considérés depuis longtemps comme étant la puissance économique la plus importante. Cependant, son avance semble diminuer d'année en année et notamment face à des géants comme la Chine, l'Inde et peut être aussi la Russie (pas certains que l'Europe rentre dans la course). Cela semble destabiliser un certains équilibre ce qui jej pense et à prendre en compte dans les évènements en cours.

Ensuite, il y a eu des négociations entre commerciales entre la Chine et les Etats-Unis et à priori cela ne se passe pas tout à fait comme prévu. Et là, très recemment il y a cette décision qui m'a fait poser beaucoup de questions (ou ouvrir les yeux). Le gouvernement des Etats-Unis demande à de nombreuses entreprises américaines en télécommunications de ne plus travailler avec certaines entreprises chinoises dont Huawei. Mais Huawei n'est pas le seul, on entend beaucoup son nom car il est l'un des constructeur les plus présents au monde derrière Samsung et Apple.

La nouvelle s'est surtout propagée lorsque Google a communiqué qu'il retirait le droit à Huawei d'utiliser ses services. Même si personnellement, je trouve cette nouvelle sans grande conséquence car je n'utilise pas leurs services et que ce serait une bonne chose que l'on puisse avoir le choix de ne pas les utiliser par défaut... mais bon ce n'est pas le sujet. 

Mais cela ne s'est pas arrêté à Google, il y a aussi ARM (qui d'ailleurs n'est pas américain mais anglais) qui est le processeur le plus présent dans les téléphones portables, notamment au travers de Qualcomm. Et ensuite, cela continue, il y a l'exclusion de la Wifi-Alliance et de l'association SD. 

Si on regarde les effets de cette annonce, pour des raisons politico-commerciales, une simple sanction commerciale décidée par la maison blanche peut nuire à des millions de personnes à travers le monde sur un outils du quotidien. Quand on entend parler de sanction c'est souvent une coupe dans l'importation de certains produits dans le marché national où a été prise la décision. Ici les sanctions des Etats-Unis affectent plus ou moins le monde entier.

De ce fait, je me demande vraiment la puissance réelle des Etats-Unis notamment dans le domaine des télécommunications. Notamment avec l'actualité, que se passerait-il en Europe si les Etats-Unis étaient également en *guerre* commerciale?

- Est-ce que par exemple, ils pourraient agir au travers d'une sanction sur le DNS au niveau mondial ? 
- Est ce que sur simple décision / sanction, les Etats-Unis pourrait interdire:
  - l'usage du GPS ?
  - l'usage / les données des géants du web (Google, Microsoft, Facebook...) <-- j'aime bien l'idée :)
  - délencher l'effacement des données de tous les téléphones Android (vu que Google peut le faire) ?

La liste de ce type de question est longue et revient à se demander s'il ne serait pas intéressant d'être indépendant d'un point de vue numérique même si cela semble très compliqué aujourd'hui? 

Je ne suis pas un grand fan de ce qui se passe en Russie mais je comprends mieux maintenant la mise en place de leur loi qui demande à ce que toutes les données russes soient hebergées localement ainsi que les tests d'isolement d'Internet qu'ils ont menés récemment.

Le Libre a certainement un rôle à jouer pour cette indépendance, de nombreuses alternatives existent mais toutes ne sont peut être pas (encore) au point et certaines sont à créer. Mais au final lorsqu'on est Libriste c'est un peu ce que l'on recherche. Il n'y aurait qu'à appliquer ce principe au niveau européen, national... oui je sais c'est pas trop la tendance, mais bon, on peut toujours espérer :)

 

