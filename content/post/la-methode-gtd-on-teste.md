--- 
title: "La Methode Gtd on Teste" 
date: 2018-02-24T21:42:43+01:00 
draft: true
---

# La méthode GTD, Get Things Done

Il y a un peu plus d'un an, [Craig Maloney](http://decafbad.net/), contributeur
de Pepper&Carrot m'a fait découvrir la [méthode
GTD](https://fr.wikipedia.org/wiki/Getting_Things_Done) de David Allen. GTD
signifie *Get Things Done* et on va dire que ce n'est pas toujours ce qui arrive
avec ce que j'entrepends.

Craig lors d'une réunion du Michigan User Group a présenté la méthode et les
outils qu'il utilisait.

Ça m'a donné envie de tester car j'avais besoin d'une méthode pour mieux
m'organiser. Bien souvent on essaie plein de super logiciels / services qui
promettent monts et merveilles ... mais comme j'ai pu le lire à plusieurs
reprises, ce n'est pas l'outils qui sera la source de la réussite mais bien la
méthode et la manière que nous l'appliquons.

Changé la vidéo avec Peertube

{{<youtube TtBCh2EWyXY >}}

L'idée ce n'est pas d'écrire un article complet sur la méthode car, en toute
transparence, je ne la maitrise pas. Par contre, j'ai pris en compte quelques
grandes lignes qui sont relativement simples à appliquer.

## La première étpae - avoir l'esprit libre

![](/content/images/2018/03/gtd_esprit_libre.jpg) [^1]

La première chose est que notre cerveau n'est pas fait mémoriser des tâches à
réaliser mais plutôt là pour réfléchir. Pour l'aider à cette tâche, il ne
faut donc pas l'encombrer avec cette grande liste de choses à faire que nous avons
tous. Il est donc préférable de l'inscrire dans un carnet, sur son ordinateur ou
tout autre endroit qui sera simple à consulter pour trier celles-ci. Cela
deviendra notre *Boîte aux lettres* (Inbox).

## La seconde étape - trier la boîte aux lettres

Cette boîte aux lettres, il faut la traiter régulièrement, afin de classer
chacune de ces tâches. Et pour cela, la méthode GTD propose une méthode
relativement simple qui se résume à ce schéma :

![](/content/images/2018/03/gtd_process.png)

C'est peut être l'étape la plus difficile. En effet, c'est à ce moment il faut
parvenir à prendre le recul nécessaire pour mettre une tâche au bon endroit. Et
pour tout avouer, je n'y parviens pas encore et je n'applique pas tout à fait le
process indiqué.

Les points importants selon moi sont : 

* tout ce qui ne pas encore prêt à être réalisé doit être suivi dans une liste
  séparée ou tout simplement mis à la poubelle.
* tout ce qui prend moins de 2min, prendra plus de temps à suivre qu'à le
  réaliser immédiatement. Il faut donc faire la tâche tout de suite.
* ce qui est déjà planifier, doit être mis dans un agenda / calendrier et doit
  être sorti de la liste.

## Les contextes et les projets

Je préviens tout de suite, je n'ai pas lu la *bible* sur le sujet, du coup je
vais être dire de grosses bêtises!  

Une tâche se réalise dans un **contexte**, dans certains outils on le préfixe
avec un `@`. Il s'agit du lieu, moment ou la tâche peut être réalisée. Dans mon
esprit, mais il ne peut y avoir qu'un seul contexte par tâche. 

Voici les contextes que j'utilise : 

* `@maison` : pour les tâches que je peux réaliser à la maison
* `@ordi`: pour les tâches que je dois faire sur mon PC
* `@tel`: pour les appels téléphoniques
* `@pro`: pour les tâches à réaliser au travail
* `@attente`: pour mes tâches qui sont en pause et attendent un évènement
  particulier.





[^1]: image [Julie Boiveau](https://julieboiveau.wordpress.com) - CC-BY-NC
