+++
author = "ValVin"
categories = ["vie-privée", "android", "degooglisons"]
date = 2016-01-18T19:17:35Z
description = ""
draft = false
featured_image = "/content/images/2016/01/keyboard-621830_640.jpg"
slug = "android-sans-google-2"
tags = ["vie-privée", "android", "degooglisons"]
title = "Android sans Google #2"

+++

Dans l'[article précédent](https://blog.valvin.fr/2015/11/05/android-sans-google-1/), nous avons vu ce qu'était les Google Apps ou GMS. Nous avons vu les premières alternatives aux applications : Play Store, Chrome, Google Search, Hangout et Google+.
Cependant, même si nous avons désactiver chacune de ses applications, il en reste encore quelques une à étudier.

Commençons par une que je n'ai pas encore évoquée car elle ne fait pas partie des GApps et pourtant cette application à la possibilité de connaître énormément de choses sur nous. Il s'agit du clavier paramétré sur votre téléphone Android, il y a de fortes chances que ce soit celui de Google. 

###Clavier Google
**Package :** `com.google.android.inputmethod.latin`
![](/content/images/2015/11/claviergoogle.png)

**Description** : 

Le clavier est l'application qui apparait pour chacune de nos saisies de texte.

On le retrouve dans le paramétrage du téléphone, dans la rubrique "Langue et Saisie". Sur le mien en Android 5.0, j'ai "Clavier Actuel" avec le nom de mon clavier. Chaque clavier, peut ensuite être paramétré dans ce même menu.
Dans les fonctionnalités que Google met en avant, il y a celle-ci :
>Mémorisation automatique des termes que vous saisissez : vous n'avez pas besoin d'ajouter des termes manuellement à un dictionnaire personnel. Saisissez un mot, puis, la fois suivante, faites un geste pour le saisir, ou retrouvez-le dans les suggestions. 

Avec la possibilité de synchroniser tout ça pour le retrouver sur les autres terminaux. De là à penser que toutes nos saisies sont sauvegardées chez Google il n'y a qu'un pas.

**Alternative proposée :** [AnySoft Keyboard](https://github.com/AnySoftKeyboard/AnySoftKeyboard)

Pour ma part, j'utilise la [version disponible sur F-Droid](https://f-droid.org/repository/browse/?fdfilter=anysoft&fdid=com.menny.android.anysoftkeyboard) à laquelle il faut ajouter le [package de langue](https://f-droid.org/repository/browse/?fdfilter=anysoft&fdid=com.anysoftkeyboard.languagepack.french_xlarge). 
Cependant, celle-ci est assez ancienne et il existe une version plus récente sur le PlayStore (à tester avec GPlayCli). Visiblement, la raison est que [F-Droid ne parvient pas à compiler](https://f-droid.org/wiki/page/com.menny.android.anysoftkeyboard) les dernières versions.
Quoi qu'il en soit, la version dispo sur F-Droid me permet d'avoir un clavier paramétrable correspondant à mes besoins. Justement en parlant de paramétrage, il ne faut pas hésiter à bien parcourir tous les menus car il est assez complet.

###Gmail
**Package :** `com.google.android.gm`

![Logo gmail](/content/images/2015/11/New_Logo_Gmail-svg-resized-resized.png)
**Description :**

Alors Gmail, c'est le service de messagerie de Google. Je ne pense pas trop me tromper en disant que Gmail a révolutionné le webmail. Pour ma part, j'ai bénéficié d'un compte en 2002 et je l'ai utilisé jusqu'en 2014. Autant dire que Google sait énormément de choses sur moi ... :-/

Niveau données personnelles, la messagerie est un excellent point d'entrée. Même s'il faut reconnaître qu'avec l'avènement des réseaux sociaux et smartphone, on n'utilise plus autant sa messagerie pour communiquer.

Il ne faut pas se leurrer, changer de messagerie n'est pas une chose facile. Car si c'est pour utiliser un autre client que Gmail mais garder ses mails au même endroit, cela ne sert à rien. Il existe donc plusieurs solutions pour changer de messagerie :

* héberger soit même sa messagerie avec son propre nom de domaine ==> réserver pour les utilisateurs avertis ; j'ai fait [plusieurs billets](https://blog.valvin.fr/tag/smtp/) sur la partie DNS mais ce n'est qu'une partie du travail que cela représente.
* choisir une messagerie respectueuse de la vie privée. On oublie donc celle de votre FAI, LaPoste, Microsoft, Yahoo & Cie. Voir [ce billet](http://spiraledigitale.com/messageries-emails-et-webmails-lequel-choisir/) qui liste les messageries respectueuses de la vie privée. Mais honnêtement, je ne les ai pas testé.
* choisir (temporairement) la messagerie de son FAI ; je suis passé par cette étape, le temps de mettre en place ma messagerie hébergée. L'avantage c'est que le FAI n'utilisera normalement pas le contenu de vos messages à des fins marketing ... par contre, il ne faut pas se mentir, les services de l'Etat (ou les boites noires) y ont accès.

Une fois ce choix fait, il existe différents clients mails Android. Pour ma part j'utilise K9-Mail.

**Alternative proposée :** [K9-Mail](https://f-droid.org/repository/browse/?fdfilter=K-9+mail&fdid=com.fsck.k9)

![K-9 Mail Logo](/content/images/2015/11/k9.png)

**K-9 Mail** est un client de messagerie très complet qui permet de gérer plusieurs compte de messagerie. Pour ma part, j'utilise différents comptes IMAP. 

J'aime bien la notion de [1ère et 2ème classe de dossier](https://github.com/k9mail/k-9/wiki/WorkingWithClasses) et la messagerie unifiée. Pour cette dernière, il est possible de regrouper différents dossiers IMAP de différents comptes ce qui permet d'avoir un aperçu des messages importants.

Au niveau configuration, cela se fait très facilement, le client interroge les DNS pour retrouver les informations à partir de l'adresse mail. Si cela ne correspond pas, il est très facile de les modifier.
Au niveau ergonomie, il y a une petite ressemblance avec l'application Gmail (en tout cas celle de 2014), et celle-ci est très intuitive.

Pour plus de détails, il y a de la documentation [ici](https://github.com/k9mail/k-9/wiki/Manual).

Cela fait quelque temps que ce billet est dans mes brouillons. Faute de temps, je n'ai pas eu l'occasion de le compléter, mais autant publié ce qui est déjà écris :)

**Crédits photos** : [Pixabay - clavier clé succès en ligne](https://pixabay.com/fr/clavier-cl%C3%A9-succ%C3%A8s-en-ligne-621830/)







