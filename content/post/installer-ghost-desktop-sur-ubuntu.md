+++
author = "ValVin"
categories = ["blog"]
date = 2016-03-25T12:10:49Z
description = ""
draft = false
slug = "installer-ghost-desktop-sur-ubuntu"
tags = ["blog"]
title = "Installer Ghost-Desktop sur Ubuntu"

+++

**UPDATE 25/02/17** : Ghost propose désormais un package pour les distributions Ubuntu / Debian.

**Ghost** vient de mettre à disposition une [version desktop](https://github.com/TryGhost/Ghost-Desktop/releases) qui d'accéder à son ou ses blogs Ghost depuis son desktop. Tout ceci grâce à Electron.
Dans l'annonce, j'ai été un peu deçu de ne retrouver, pour la version 0.1.1, des builds pour windows (32 et 64bits) et MacOS X. Vu que je suis sous Ubuntu ... ça ne le faisait pas.
Mais l'avantage de tout produit OpenSource, c'est que si ça n'existe pas on peut (esasyer de) le faire nous même.

**EDIT 2016/06/27** : maintenant, Ghost propose directement un .deb, ce qui rend le fonctionnement beaucoup plus simple : c'est par [ici](https://github.com/TryGhost/Ghost-Desktop/releases/tag/v0.5.1)

Le but du billet et donc de partager mon expérience même si ça reste très simple :

## Les pré-requis

Il n'y a pas vraiment de procédure d'installation, j'ai donc fait comme pour toute application NodeJS, un bon `npm install`.
Bien entendu, ça n'a pas marché :)

Ma version npm et node était bien à jour mais ça butait sur le module `keytar@3.0.0`.

```
npm ERR! Linux 4.2.0-34-generic
npm ERR! argv "/usr/local/bin/node" "/usr/local/bin/npm" "install"
npm ERR! node v5.4.1
npm ERR! npm  v3.3.12
npm ERR! code ELIFECYCLE

npm ERR! keytar@3.0.0 install: `node-gyp rebuild`
npm ERR! Exit status 1
npm ERR! 
npm ERR! Failed at the keytar@3.0.0 install script 'node-gyp rebuild'.
```
Après lecture de ce [ticket](https://github.com/atom/node-keytar/issues/18), il me manquait le package `libgnome-keyring-dev`

```
sudo apt-get install libgnome-keyring-dev
```

Effectivement après ce petit ajout tout se passe bien.

Ensuite, pour la compilation, il est nécessaire d'avoir ember-cli d'installer :

```
sudo npm install -g ember-cli
``` 

## la compilation

Je ne suis pas très à l'aise avec Grunt mais en lisant le fichier `Gruntfile.js`, j'ai compris que si je voulais un livrable pour linux, il fallait lancer :

```
grunt build
```

Mais la première fois ça n'a pas été suffisant, car comme toute application web, il faut récupérer les dépendances bower.

```
bower install
```

Donc si je récapitules :

```
sudo apt-get install libgnome-keyring-dev
sudo npm install -g ember-cli
npm install && bower install
grunt build
```

Et hop, ensuite on le petit excutable `Ghost` qui se trouve dans `./electron-builds/Ghost-linux-x64`

![](/content/images/2016/03/ghost-desktop-0-1-1.png)

Je suis sur que dans les prochaines versions, nous aurons le package de compiler par défaut ;)