+++
author = "ValVin"
categories = ["android", "degooglisons", "vie-privée"]
date = 2015-11-05T13:02:51Z
description = ""
draft = false
featured_image = "/content/images/2015/11/android-994910_1280.jpg"
slug = "android-sans-google-1"
tags = ["android", "degooglisons", "vie-privée"]
title = "Android sans Google ? #1"

+++

[Android](https://source.android.com/) est un projet Opensource (AOSP) dont Google est le leader. Ils livrent les sources de son système dès que qu'une nouvelle version est disponible. Récemment ce fut le cas pour Android 6.0 (Mashmallow). Cependant, ce projet ne contient pas les applications Google que l'on retrouve sur tous les smartphones Android. Il s'agit des [Google Apps](https://www.google.fr/mobile/android/) ou Google Mobile Services (GMS)

![Google Apps](/content/images/2015/10/googleApps.jpg)
Dans ces fameuses Google Apps, on retrouve tout l'écosystème de Google : Gmail, Google Drive, Youtube, Chrome ... et bien entendu le **PlayStore**.
D'ailleurs de ce côté, ils ont été très malins, car sans le PlayStore, on perd l'accès à un très grand catalogue d'applications. Du coup, les constructeurs, se doivent de livrer un téléphone avec celui-ci. 
**MAIS :** 

1. si tu veux installer le PlayStore, il faut **installer l'ensemble des Google Apps** ! C'est la raison pour laquelle, très peu de téléphone Android sont livrés sans l'écosystème Google.
2. les constructeurs ne peuvent pas installer les Google Apps sans **obtenir une licence**:
>After building an Android compatible device, consider licensing Google Mobile Services (GMS), Google’s proprietary suite of apps (Google Play, YouTube, Google Maps, Gmail, and more ) that run on top of Android. GMS is not part of the Android Open Source Project and **is available only through a license with Google**. For information on how to request a GMS license, see Contact Us.

3. sur un téléphone non rooté, toutes les applications systèmes ne peuvent pas être désinstallées. Et les Google Apps sont dans 99% des cas, des applications systèmes.

Et le souci de tout ça, c'est que l'écosystème Google et la notion de vie-privée ne font pas bon ménage ! D'ailleurs souvent le constructeur ne se gêne pas non plus. 
Et sur son smartphone, les données privées, ce n'est pas ce qu'il manque :

* Contacts
* Appels téléphoniques
* SMS
* Mails
* Photos
* Position GPS et Wifi

Avec ce préambule, on se dit qu'on est coincé, ils ont bien verrouillé leur affaire ... Ce n'est pas tout à fait faux mais il existe tout de même des solutions.

Il existe trois solutions "radicales" ou plutôt pour les plus téméraires :

1. ne plus utiliser la version d'Android de son constructeur et s'orienter vers une version OpenSource compatible avec son matériel, la plus connue étant Cyanogen. Vigilance cependant car même si la société fourni une version 100% open-source, elle établi de plus en plus de contrats commerciaux avec des sociétés comme Spotify, Microsoft ... ([Article à ce sujet](http://www.frandroid.com/android/321385_cyanogen-inc-lautre-visage-dandroid-qui-derange-google))
[![](/content/images/2015/10/cyanogen.png)
](http://www.cyanogenmod.org/)


2. rooter son téléphone, c'est à dire obtenir les droits administrateurs (root). Car effectivement, sur les smartphones en général, on n'est qu'utilisateur, l'administrateur c'est le constructeur ou l'éditeur.
![](/content/images/2015/11/supersu-resized-resized.png)
3. s'orienter vers un OS open-source plus respectueux de la vie privée comme Firefox OS ou encore Ubuntu Phone
![](/content/images/2015/11/firefoxos-resized-resized.png)

Ou bien, si l'on ne veut pas prendre de risque pour ne pas casser la garantie de son téléphone ou tout simplement pour ne pas le réinitialiser. A partir d'Android 4.4, on peut bloquer certaines applications même système. Cela permet de bloquer les GApps au fur et à mesure. Dans un premier temps, on peut commencer par les GApps que l'on ne se sert pas ou que l'on peut facilement contourner.

Dans ce premier billet, je vous présente les principales GApps et leurs alternatives. Il s'agit du Play Store, Chrome, Google Search, Hangout et Google+. 
J'indique pour chaque GApps son package afin de faciliter son blocage (via adb), sa description et l'alternative proposée.

###Store d'application : Play Store
**Package :** `com.android.vending`
![](/content/images/2015/11/playstore.png)

**Description :** Tout possesseur d'Android connait le Play Store, il s'agit de l'application qui permet de parcourir 98% des applications de les installer et de les mettre à jour.

**Alternative proposée :** [F-Droid](https://f-droid.org/)
![](/content/images/2015/11/fdroid.png)

F-Droid est une application permettant de gérer différents catalogues d'applications. Elle est disponible avec le catalogue F-Droid qui contient une liste d'applications 100% open-source. 
Le catalogue du [Gardian Project](https://guardianproject.info/apps/) est également disponible mais non activé par défaut.

Il est également possible de rajouter d'autres catalogues d'applications. Sur [Diaspora](https://framasphere.org/posts/1161793), on m'a conseillé cet [article de Matlink](https://linuxfr.org/news/gplaycli-et-gplayweb-profiter-de-google-play-store-sans-installer-les-google-apps) qui permet de générer un catalogue d'applications F-Droid ayant comme source le Play Store. Je n'ai pas encore créé mon propre catalogue mais ça ne saurait tarder :)

###Browser Web : Google Chrome
**Package :** `com.android.chrome`
![](/content/images/2015/11/chrome.png)

**Description :** Chrome est bien connu car il est probablement, après le moteur de recherche, le produit Google le plus utilisé.Il s'agit du navigateur web qui dispose d'une très grosse part de marché sur les postes de travail.

**Alternative proposée** : [Firefox](https://www.mozilla.org/fr/firefox/android/)
![](/content/images/2015/11/firefox.png)

Petite vigilance avec Firefox est qu'il est amené à [disparaître de F-Droid](https://f-droid.org/wiki/page/org.mozilla.firefox) car :

* il promeut des applications non gratuite
* contient des librairies non libres
* envoie des rapports et vérifie les mises à jour sans la permission de l'utilisateur

Il est cependant toujours possible d'avoir les mises à jours en utilisant [FFUpdater](https://f-droid.org/repository/browse/?fdfilter=ffupdater&fdid=de.marmaro.krt.ffupdater) ou encore utiliser [Fennec](https://f-droid.org/repository/browse/?fdfilter=fennec&fdid=org.mozilla.fennec_fdroid) qui est Firefox recompilé par F-Droid avec uniquement les composants 100% open-source.

###Recherche : Google Search
**Package :** `com.google.android.voicesearch`
![](/content/images/2015/11/googlesearch.png)

**Description :** Cette application permet d'effectuer différents types de recherches sur votre téléphone. Elle s'active en effectuant un appui long sur le bouton Home. Elle effectue des recherches sur Google mais également sur le contenu du téléphone (contacts, applications ...). 

Attention, certaines applications on des dépendances avec celle-ci. Je pense notamment à l'application téléphone qui sur mon téléphone était utilisée par défaut pour la recherche contact. Quand je recherchais un contact après l'avoir désactivé, l'application téléphone crashé.

**Alternative proposée :** Duck Duck Go / [FAST App Search Tool](https://f-droid.org/repository/browse/?fdfilter=fast&fdid=org.ligi.fast)
![](/content/images/2015/11/fast.png)
Personnelement j'utilise [FAST](https://github.com/ligi/FAST) en tant qu'application de recherche. Cela permet de l'activer avec un appuie long sur le bouton "Home". Fast permet de rechercher uniquement dans mes applications mais cela correspond à mon besoin. FAST permet également de remplacer le Launcher mais je n'ai pas choisit cette solution.
![](/content/images/2015/11/ddg.png)
Pour les recherches web, il y a l'application Duck Duck Go disponible sur F-Droid.

###Social : Hangout et Google+

####Hangout
**Package :** `com.google.android.talk`
![](/content/images/2015/11/hangout.png)

**Description :**
Hangout, initialement Gtalk, est l'application permettant d'utiliser le système de chat Google que l'on retrouve dans l'interface web Gmail.
Récemment,Hangout peut également être l'application permettant de lire / écrire les SMS tout en les fusionnant aux différentes conversations sur GTalk.

**Alternative proposée :** [Conversation](https://f-droid.org/repository/browse/?fdfilter=XMPP&fdid=eu.siacs.conversations) pour la partie messagerie instantannée / [SMS Secure](https://f-droid.org/repository/browse/?fdfilter=smssecure&fdid=org.smssecure.smssecure) pour la gestion SMS
![](/content/images/2015/11/conversation.png)
Pour la partie messagerie instantanée, je propose Conversation qui est un client XMPP. Il est possible de paramétrer Hangout mais ce n'est pas vraiment l'objectif. Il sera donc nécessaire de se créer un compte XMPP sur un serveur dont [voici une liste](http://wiki.jabberfr.org/Serveurs).
Je recommande la lecture des [billets de Goofy sur XMPP](http://www.goffi.org/tag/parlons_xmpp).
![](/content/images/2015/11/smssecure.png)
Pour la gestion des SMS, il est possible de conserver l'application SMS d'Android. Cependant, de mon côté, j'utilise [SMS Secure](https://smssecure.org/) qui permet d'échanger des SMS chiffrés avec les personnes disposant de la même application tout en restant compatible avec ceux qui ne la dispose pas. L'avantage c'est que le stockage des SMS est chiffré et il est possible de définir un code PIN pour accéder à l'application.

####Google+ :
**Package :** `com.google.android.apps.plus`
![](/content/images/2015/11/googleplus.png)
**Description :** Google+ est le réseau social de Google qui a souhaité "concurrencer" Facebook / Twitter. Même si Google a fait un énormément d'effort pour "forcer" l'utilisation de son réseau social, on ne peut pas dire qu'il a été une très grande réussite.

**Alternative proposée :** [Diaspora Native Web App](https://f-droid.org/repository/browse/?fdfilter=diaspora&fdid=ar.com.tristeslostrestigres.diasporanativewebapp)
![](/content/images/2015/11/diaspora.png)
[Diaspora](https://diasporafoundation.org/) est un réseau social basé sur trois valeurs : décentralisé, liberté et confidentialité.
Framasoft propose un pod [Framasphère](https://framasphere.org/) sur lequel je vous conseille de vous inscrire. Pour ma part, je suis [ici](https://framasphere.org/u/valvin)

Voici pour ce premier billet concernant la degooglisation de son téléphone Android. Dans les prochains billets, on parlera de GMail, Maps et Street View et les autres Gapps pas encore abordé.

*Sources:*
Pixabay : [androïde-linux-guimauve-smartphone](https://pixabay.com/fr/andro%C3%AFde-linux-guimauve-smartphone-994910/)
