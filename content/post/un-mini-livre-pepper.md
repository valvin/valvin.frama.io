+++ 
author = "ValVin" 
date = 2018-03-03T10:00:24Z 
description = "Un mini-livre Pepper&Carrot sur une feuille A4" 
draft = false
slug = "un-mini-livre-pepper-et-carrot" 
title = "Un mini-livre Pepper&Carrot pour les épisodes \"muets\""
featured_image = "/content/images/2018/03/cover_minibook.jpg"
+++

Grâce à Pepper&Carrot, j'ai découvert [Scribus](https://www.scribus.net/) qui
permet de réaliser entre autres des supports à destination des imprimeries
(flyers, posters, livre, bd ...).

David Revoy a travaillé au mois de juin dernier sur le [livre 1 de
Pepper&Carrot](https://framagit.org/peppercarrot/book) afin que la Fondation
Krita édite une version spéciale anglophone. Ce projet a été réalisé avec
Scribus et ma curiosité m'a poussé à connaitre un peu mieux l'outils.

En parallèle, dans un de mes livres [*Labo
BD*](https://www.eyrolles.com/Loisirs/Livre/labo-bd-9782212136302) des éditions
Eyrolles, j'ai découvert comment concevoir un mini-livre sur une feuille A4
comme l'image ci-dessous l'illustre :

![](/content/images/2018/03/minibook_A4_vincent_pantaloni.png)

*[Vincent Pantoloni -
CC-BY](https://www.overleaf.com/latex/examples/mini-livre-exercices-de-rentree-en-seconde/gncxgskzswtc)*

  En vidéo, ce peut être plus facile de comprendre le pliage :
[ici](https://www.youtube.com/watch?v=21qi9ZcQVto)

  Ceci m'a donné l'envie de faire cet exercice avec les épisodes muets de
Pepper&Carrot. Pour les autres épisodes, même si rien n'empêche de les utiliser,
la lecture des dialogues peut s'avérer compliquée. Les épisodes muets sont
publiés tous les 5 épisodes (5, 10, 15, 20 et bientôt 25). Mon préféré est
l'épisode 15 très certainement à cause de la boule de crital bleue.

  Bon par contre, on n'a que 8 pages, du coup faut serrer un peu, on ne pourra pas
avoir une couverture.

  L'idée c'est d'avoir un projet scribus permettant d'imprimer la mini-BD très
facilement. J'ai donc repris le concept et je l'ai intégré dans scribus en
plaçant les pages dans le bon ordre et le bon sens. 
  ![template
scribus](https://framagit.org/valvin/peppercarrot_minibook/raw/master/doc/scribus_preview_template.png)

  Maintenant que l'on a ce template, il faut pouvoir l'alimenter avec l'épisode
souhaité. Pour cela, j'ai souhaité automatiser ce qui l'était.

  Tout d'abord, j'ai repris le script que j'avais réalisé pour générer des
fichiers [CBZ - Comic Book
Archive](https://fr.wikipedia.org/wiki/Comic_book_archive) tout en l'adaptant
légèrement. Pour ceux que ça intéresse, le projet est
[ici](https://framagit.org/valvin/peppercarrot_episode_downloader).

  Dans scribus, j'ai rajouté 8 images qui deviendront par la suite nos 8 pages.
Je les ai liées dans le répertoire `./pages` qui est alimenté par le
téléchargement de l'épisode souhaité. On retrouvera après téléchargement les
fichiers `./pages/P01.png`, `./pages/P02.png`…

  Dans scribus, si on a téléchargé l'épisode 15, on obtient ceci :

  ![vue scribus episode
15](https://framagit.org/valvin/peppercarrot_minibook/raw/master/doc/scribus_preview_ep15.png)

  Pour que cela soit plus *simple*, j'ai fait un petit script qui permet de
lancer le téléchargement de l'épisode, lancer scribus et la génération du PDF
correspondant. Donc si on lance la commande suivante `./generate_mini_book.sh 15
en`, on obtient le résultat suivant :

  <iframe width="560" height="315"
src="https://peertube.valvin.fr/videos/embed/b2cc97ef-dc05-400b-94f8-43ea61237718"
frameborder="0" allowfullscreen></iframe>

  Voici le résultat final :

<iframe width="560" height="560"
src="https://peertube.valvin.fr/videos/embed/fef4ca2e-1711-4694-a087-2da8c625c8d9"
frameborder="0" allowfullscreen></iframe>

Si cela vous intéresse, le projet est disponible sur ce dépôt : [Pepper&Carrot Minibook](https://framagit.org/valvin/peppercarrot_minibook)

Il reste encore un peu de travail comme intégrer titre de l'épisode avec la
locale souhaitée ou encore gérer une disposition différente en fonction du nombre
de page dans l'épisode.

Vous verrez c'est très ludique et le système de pliage peut être repris pour
d'autres projets.

![](/content/images/2018/03/minibook_scribus.jpg)
