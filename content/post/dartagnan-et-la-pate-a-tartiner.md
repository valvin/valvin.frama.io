---
title: "Dartagnan et la pâte à tartiner"
date: 2018-01-29T12:57:13+01:00
featured_image : "/content/images/2018/01/dartagnan-cover.jpg"
tags: ["vie-privée", "gafam"]
slug: dartagnan-et-la-pate-a-tartiner
draft: false
---

La semaine dernière,  un évènement qui s'est déroulé principalement dans le nord de la France m'a interpellé. Il s'agit de la promotion à -70% d'une marque de pâte à tartiner dans une grande surface alimentaire.[^1] Pour résumer la scène, des personnes sont prêtes à se battre pour obtenir 3 pots de ce produit dont rafollent les enfants. 

Je pense que la scène reflète un réel malaise de la société mais ce n'est pas là ou je veux en venir. Car effectivement, je voudrais faire un parallèle avec mon combat au sujet de la vie privée, de la publicité / profilage en ligne. Dans ce combat, on entend souvent l'argument **"*Je n'ai rien à cacher*"** , dans ce cas, ce serait plutôt **"*De toutes façons, la publicité n'a aucun effet sur moi*"**.  Et même si l'exemple est extrême, force est de constater que le marketing arrive à *manipuler* des personnes avec ce genre d'évènement. 

Comme je l'ai entendu, le coup était bien préparé :

* La marque est l'une des préférées des familles
* Les quantités étaient limitées
* Une limite d'achat a été fixé à trois unités
* Et bien entendu l'offre était importante : -70%

Bien entendu avec ce genre d'offre, pas sûr que l'enseigne dégage une marge directe très importante. Cependant, quand on se rend dans une grande surface tout est fait pour que l'on reparte avec le plus d'articles possibles, voire peut être dans ce cas, faire en sorte que de nouveaux clients changent leurs habitudes ponctuellement pour réaliser leur courses hebdomadaire. Sans parler le bouche à oreille et toute la publicité *gratuite*. Un effet bon affaire à ne pas louper.

Bref, l'objectif c'est de faire en sorte que le consommateur afin qu'il dépense plus (Une sorte de définition du marketing). Et sur ce coup, je pense qu'ils ont poussé le bouchon un peu loin. D'autant que ceux qui se sont déchirés pour avoir sont peut-être des personnes dont le seul objectif était de faire plaisir à leurs enfants car pour une fois, cette marque était abordable. J'ose espérer que le buzz généré n'est au final pas si positif que cela, mais pas sûr... 

Bien entendu, ce n'est pas nouveau! C'est un peu comme pour les soldes en général, les lancements du dernier produit de la marque à la pomme, ou tout autre coup de comm' que les agences se font un plaisir de mettre en place.

Mais cela me semble malgré tout similaire à ce que l'on voit sur internet. Que ce soit un réseau social qui tente par tous les moyens de faire rester l'internaute sur son site. Ou encore, la démultiplication des services afin que quoiqu'on fasse, un service performant et novateur soit disponible pour le geste à accomplir. Dans ces deux l'objectif est le même : enfermer l'utilisateur dans une bulle afin de mieux le connaître, mieux capter son attention et pouvoir lui passer les messages que des enseignes, marques, politiques, etc. sont prêts à payer à prix d'or. Et quoi qu'en dise ceux qui sont dans cette bulle, ils sont bel et bien manipulés.

![](/content/images/2018/01/dartagnan-pate-a-tartiner.jpg)

*fait sur du papier 90g/m2 qui n'a pas trop aimé l'aquarelle mais pour selon, il a bien tenu le choc*


[^1]: [article sur France TV Info](https://www.francetvinfo.fr/economie/intermarche-la-ruee-sur-le-nutella_2580554.html)
