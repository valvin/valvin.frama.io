+++
author = "ValVin"
categories = ["vie-privée", "dessin"]
date = 2017-04-14T18:40:05Z
description = ""
draft = false
featured_image = "/content/images/2017/04/privamics_header.png"
slug = "privamics-01-rien-a-cacher-mini-bd-sur-la-vie-privee"
tags = ["vie-privée", "dessin"]
title = "Privamics - #01 - Rien à cacher, mini-BD sur la vie privée"

+++

Grâce au projet **Pepper&Carrot** et sa communauté, j'ai repris l'envie de dessiner. Bien entendu, je n'ai pas un talent artistique très élevé mais comme j'ai plusieurs passions qui me tiennent beaucoup à coeur, pourquoi pas les lier ? 

Je me suis lancé le défi, non pas de faire une BD mais plus une série de *strip* sur la thématique de la **vie-privée**.
L'idée est donc de mettre en avant les dangers du non respect de la vie-privée en vulgarisant avec un peu d'humour. Par contre, je dois avouer que j'ai un parti pris, je ne suis pas certains de faire dans la nuance ... mais je vais essayer de ne pas non plus être extrêmiste dans mes propos. 

J'ai décidé de nommer ce projet **Privamics** pour *Privacy Comics*.

Je vous propose donc le premier épisode de **Privamics** : 

## Rien à cacher 

[![](/content/images/2017/04/rienacacher-e01_web.png)](https://blog.valvin.fr/content/images/2017/04/rienacacher-e01_web.png)
![](/content/images/2017/03/by-sa.png)

## Nothing to hide
[![](/content/images/2017/04/rienacacher-e01_en_web.png)](/content/images/2017/04/rienacacher-e01_en_web.png)
![](/content/images/2017/03/by-sa.png)
Ce contenu est mis à votre disposition sous licence [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).
Les sources sont disponibles sur Framagit : [ici](https://framagit.org/valvin/privamics)

Je remercie tout particulièrement David Revoy (auteur de Pepper&Carrot), Craig Maloney et Nicolas Artance (contributeurs de Pepper&Carrot) qui m'ont apportés de nombreux conseils et m'ont encouragés à de nombreuses reprises.


