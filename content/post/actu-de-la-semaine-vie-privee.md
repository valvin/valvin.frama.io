---
title: "Vie privée, actus de la semaine"
date: 2017-11-24T13:12:39+01:00
featured_image: "/content/images/2017/big-brother-2783030_1280.jpg"
tags: ["vie-privée","framasoft"]
slug: vie-privee-actus-de-la-semaine
draft: false
---

Cette semaine a été riche en actualité. Non pas en raison de ce fichu *Black Friday* que je n'apprécie guère mais au sujet des différentes informations autour de la vie privée.

## Google collecte votre position sans notre accord

Depuis début 2017, Google collecte les données des identifiants des antennes relais en permance. Que la localisation soit activée ou non et même dans le cas ou vous ne disposez pas, peu importe, la donnée est collectée.

Mais le gentil Google, nous informe que bien qu'il n'ait pas demander l'autorisation à ces utilisateurs, il n'a pas stocker cette donnée qui bien entendu était remontée de manière anonyme. L'unique but était d'améliorer les performances de la délivrance des notifications ... 

Mouai, croit ça que veut bien! Google collectant une donnée sans l'exploiter jusqu'à la dernière goutte, je n'y crois pas une seconde!

[source](https://qz.com/1131515/google-collects-android-users-locations-even-when-location-services-are-disabled/)

Le sujet m'a inspiré ce petit dialogue à partir de [Gknd-Creator](https://framalab.org/gknd-creator/) : 

![](/content/images/2017/gknd_google_localisation.png)

## Uber reconnaît s'être fait piraté quelques 57 millions de comptes

Et tout ça il y a un an ... Effectivement, Uber a tenté de faire en sorte que l'information ne soit pas révélé, ils ont même verser 100 000 € afin que le pirates détruisent ces données. 

Il s'agit des données personnelles de clients et de chauffeurs dont des numéros de permis américains.

Pas certains que jouer le jeu des pirates soit la bonne manière de gérer ce genre d'incident. Et personnellement j'aurais une confiance limité sur la destruction des données. D'ailleurs, je me demande ce qui a poussé a révélé l'information un an plus tard...

[source](https://www.nextinpact.com/news/105666-uber-57-millions-comptes-pirates-100-000-dollars-pour-etouffer-affaire.htm)

## L'utilisation de captures de frappes et de souris sur de nombreux sites populaires

Bien que je ne suis pas certains que ce soit une nouveauté, j'ai lu cette information cette information cette semaine. Pour faire simple, des sites bien connus utilisent eux même ou au travers de tiers à une technique permettant de récupérer les frappes claviers et les mouvement de souris en permanence lorsque vous naviguer sur leur site web.

Je trouve le concept un peu flippant car ça revient à *regarder* par dessus votre épaule lors de votre naviguation. L'air de rien, on peut constater pas mal de chose avec cette techniquemais le plus vicieux c'est que dans certains cas, même des mots de passes sont capturés. En lisant l'article j'imaginais la pesonne ne se souvenant pas de son mot de passe, qui essaie tout ses mots de passes habituels. Ces données sont remontées le plus souvent à un tiers ... quid de ce qu'il sera fait de cette données.

En échangeant sur [diaspora](https://framasphere.org/posts/4171171), on m'a donné ce site qui couvre la partie souris uniquement mais qui reste très révélateur sur ce genre de pratique: [ici](https://clickclickclick.click/) 

[source](https://thehackernews.com/2017/11/website-keylogging.html)

## Google Home et copains du même genre en force pour Noël

Ce n'est pas vraiment une nouvelle mais il semblerait que les petits gadgets Google Home ou Amazon Echo vont être sous le sapin de Noël 2017. Je trouve ça un peu effrayant de laisser rentrer ces géants de la collecte de la données personnelles dans sa maison. Mais visiblement ça pourrait fonctionner.

Pour ceux qui ne connaissent pas, ceux sont de petits objets que l'on installe dans la pièce à vivre, que l'on commande par la voix. Une sortie de Siri ou Google Now mais cette fois dédié à cet usage uniquement. Ce petit gadget est ultra connecté et permet de vous donner les informations utiles (ou pas) tel quel le temps, le trafic, vos derniers rendez-vous. Mais aussi lancer la musique, car il s'agit également d'enceinte et piloter certains objets domotique de la maison.

Un mouchard de plus pour la maison cette fois ... jusqu'où iront-ils ?

## Framatube est dans le pipe !

Une bonne nouvelle, pour la fin! 

Framasoft s'investi dans le développement d'une alternative au service Google Youtube. Car même si on est très vigilent sur les services qu'on utilise sur internet, il y a bien un moment où l'on a besoin d'une vidéo et dans de nombreux cas elle est sur la plateforme de notre *ami* Google. Une telle plateforme étant très gourmandes en ressources (stockage, bande-passante), elle est très couteuse à mettre en place. Cette alternative s'appelle Peertube. Elle propose un service de diffusion de vidéo décentralisé ou chacun pourra rejoindre le réseau en créant sa propre instance (le même principe que la messagerie mais aussi Diaspora, Mastodon ...) mais aussi bénéficie de la technologie pair à pair qui permet de télécharger et partager la vidéo avec tous les utilisateurs qui visionne actuellement la vidéo. 

Une solution qui s'avère très prometteuse c'est pourquoi l'association Framasoft a embauché le développeur de la solution afin qu'il puisse s'y consacrer pleinement. Bien entendu, si l'idée vous séduit, il ne faut pas oublier que Framasoft est financé au travers des dons de chacun, n'hésitez pas à y participer, c'est par [ici](https://soutenir.framasoft.org/) 

Cela m'a insipiré ce petit "gribouillage" :

![](/content/images/2017/20171124_framatube.jpg)

[source](https://framablog.org/2017/11/22/framatube-aidez-nous-a-briser-lhegemonie-de-youtube/)
