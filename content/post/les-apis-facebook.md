---
title: "Les apis facebook et le contenu public"
date: 2021-04-25
tags: ["gafam","rss", "facebook"]
---

Utilisateur de [RSS Bridge](https://github.com/RSS-Bridge/rss-bridge) pour lire
le contenu **public** des pages Facebook, je ne peux plus accéder à ces
contenus suite à un changement de la plateforme.  C'est en lien avec ce
[ticket](https://github.com/RSS-Bridge/rss-bridge/issues/2047). Le problème est
simple: il n'est plus possible d'accéder au contenu d'une page Facebook depuis
un serveur sans devoir s'authentifier avec un compte Facebook. Techniquement
cela signifie que le contenu public créé sur Facebook n'est accessible que pour
les utilisateurs Facebook. Malheureusement rien de si étonnant que ça, les
plateformes petits à petits vérouillent ce qu'elles ont réussi à obtenir au fil
des années. Gagner de l'argent avec leurs utilisateurs n'est pas suffisant, il
ne faut surtout pas que d'autres puissent obtenir les contenus qu'ils
produisent. Et il n'y a pas Facebook qui joue à ce petit jeu. D'ailleurs, comme
vous pourrez le lire dans le ticket sur *RSS Bridge*, des utilisateurs ont
tenté des contournements pour obtenir malgré tout le contenu et ceux sont
retrouvés avec leur compte Facebook bloqué.

Naïvement, hier je me suis lancé dans l'idée qu'au travers des API Facebook il
serait possible d'accéder à ce type d'information publique sans trop de
difficultés. A la lecture de la documentation c'est effectivement le cas.
Facebook propose une API *GraphQL* avec un point de terminaison
`/<page_id>/feed`. Allez hop! Je me créé un compte sur Facebook, je
m'enregistre en tant que Développeur, je créé une application et lance
l'explorateur d'APIs. Tout semblait trop facile... mais bon, il ne fallait pas
trop rêver. Pour accéder à la donnée, il faut demander certaines autorisations
et celle qui m'intéresse est: `Page Public Content Access`:

> La fonctionnalité Accès au contenu public de la Page autorise votre app à
> accéder à l’API Pages Search et à lire les données publiques de Pages pour
> lesquelles vous n’avez pas les autorisations pages_read_engagement et
> pages_read_user_content . Les données accessibles incluent les métadonnées
> professionnelles, les commentaires publics et les publications. Vous pouvez
> utiliser cette fonctionnalité pour fournir un contenu public anonyme et
> agrégé à des fins d’analyse et de benchmarking concurrentiels. Vous pouvez
> également utiliser cette fonctionnalité pour demander des statistiques
> analytiques afin d’améliorer votre app et à des fins de marketing ou de
> publicité, par l’intermédiaire de l’utilisation d’informations agrégées,
> désidentifiées ou anonymisées (sous réserve que ces données ne puissent pas
> être à nouveau identifiées).

J'en fais donc la demande, et là ... c'est le début du parcours du combattant.
Il faut faire vérifier son application et remplir différents formulaires en
expliquant l'objectif de l'application. C'est ce que j'essaie de m'appliquer à
faire mais j'arrive à la demande suivante:

> Fournissez une vidéo par étape qui montre l’ensemble du processus par lequel
> votre app utilise cette autorisation ou fonctionnalité pour que nous
> puissions confirmer que vous en faites une utilisation correcte et qui ne va
> pas à l’encontre de nos règlements 

On me demande donc de faire une vidéo d'une solution que je n'ai pas encore
développée et pour laquelle je n'ai aucun moyen d'accéder à la donnée qui me
permettrait de le faire. Le serpent qui se mord la queue! 

Et encore, ici on ne parle que des *Pages*, pour le contenu public d'un profil
utilisateur c'est bien pire!

Ce n'est malheureusement qu'à moitié étonnant, en regardant les apis exposées
c'est orienté publicité, marketing. Il assez facile de produire du contenu sur
une Page et la gérer. Cependant obtenir du contenu même public est mission
impossible.

Petite anecdote qui n'est pas en lien direct avec l'article mais le fait de
m'être créé un compte développeur, j'ai du renseigner mon numéro de téléphone
portable. C'est impressionnant le nombre de personnes qui ont envoyé cette
donnée à Facebook car la liste d'amis qui m'ont été proposé ne pouvait se faire
qu'au travers de cette information. Du coup même si on essaie de faire
attention sur les données que l'on fournit, d'autres, involontairement les
fournissent.

Cet épisode rappelle une nouvelle fois la bonne pratique de ne pas produire du
contenu directement sur les plateformes (sans pour autant s'en
éloigner). C'est un peu moins "facile" mais des outils existent pour créer son
contenu par exemple sur son propre site et l'envoyer sur différentes plateformes
(Facebook, Twitter, Instagram...). Cela permet d'être propriétaire de son
contenu ce qui n'est malheureusement pas le cas sur les plateformes. Ceux sont
eux qui choisissent qui a le droit de lire et comment.

Encore une fois, reprenez le contrôle! :)

