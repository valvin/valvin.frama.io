---
title: "Réflexion sur la défense de la vie privee"
date: 2018-11-09T21:55:28+01:00
tags: ["vie-privée", "gafam"]
slug: reflexion-sur-la-defense-de-la-vie-privee
draft: false
---

J'écris cette note car je me rends compte que tenir une position non extrémiste
au sujet de la vie privée n'est pas toujours évidente. J'ai le sentiment que
l'omniprésence de la publicité et du marketing en tout genre rend inaudible le
message que j'essaie de faire passer au sujet du respect de la vie privée. Très
certainement je n'emploie pas les bons moyens, les bons arguments mais bon
faut-il pour autant baisser les bras ? 

D'autant que le sujet semble préoccuper
de plus en plus le grand public. On parle de plus en plus du grand méchant "F"
tout bleu, cela a commencé par l'affaire Cambridge Analytica au sujet de
l'exploitation de données issues du réseau social afin de changer la tendance
des éléctions américaine [^1]. Il y a eu aussi la fuite de données de 50 
millions d'utilisateurs. Je trouve cependant dommage qu'on ne se focalise que
sur une seule société alors que bien d'autres en font autant et de nouveaux
arrivent sur le marché. Mais les affaires *Facebook* permettront-elles d'ouvrir
les yeux au plus grand monde de ce qu'est réellement l'envers du décors ? 

A ma grande surprise le numéro l'émission *Google, 
Apple, Facebook, les nouveaux maîtres du monde* du 01 novembre sur France 2 
m'a agréablement surpris. Ce n'est pas tous les jours que l'on traite du sujet à une heure de 
grande écoute. Dommage que la partie intéressante se trouve principalement à la
fin du documentaire. Malheureusement à l'heure ou j'écris, la vidéo n'est plus
disponible [^2]

Les deux arguments que j'entends le plus souvent au sujet de l'utilisation de
services susceptibles d'exploiter des données privées sont :

> "Ils peuvent les collecter mes données, de toute façon ils n'en tireront pas
  grand chose"

> "Je sais effectivement que le(s) service(s) collecte(nt) mes données mais j'en
  suis conscient."

> "De toute façon la publicité n'a pas d'effet sur moi. Je suis maître de mes
  actes."

Pour les deux premiers, ce qu'il faudrait mettre le plus en avant c'est que ce
n'est pas la donnée unitaire qui est importante car a elle toute seule, elle n'a
effectivement qu'un intérêt limité. C'est le fait de croiser les données avec
d'autres sources. Ce ne sera pas uniquement réalisé par le service en question
mais aussi par un tiers à qui la donnée aura été monétisée. Ce croisement de 
données peut concerner l'individu lui même ou bien une population d'individus. 
Il permet d'appliquer des modèles qui permettront d'en ressortir une 
information à valeur ajoutée bien différente que les données unitaires. Ce
mécanisme est très bien illustré dans le film *Nothing to hide* [^3][^4]
lorqu'on met en place volontairement des collecteurs de données sur PC et smartphones
d'un membre de l'équipe du film plutôt sceptique. Ces données seront ensuite
analyser par un *Data scientist* et restituer à cette même personne. Le résultat
est assez surprenant.

Avec la popularisation du *Big Data*, de l'intelligence artificielle et 
notamment le machine learning, on peut facilement constater l'importance que 
consitue les banques de données obtenues par un grand nombre de service en 
ligne. Je ne me souviens pas de la source mais on dit :

> "Le pétrole de demain sera la donnée."

Du coup à qui va profiter toutes ces données collectées?

Pour le troisième point, j'ai envie de dire avec ironie : 

> "C'est pour ça que les services  marketing dépensent des milliards d'euros 
> chaque année dans la publicité. Juste parce que ça n'a aucun effet, pour le 
> plaisir de dépenser de l'argent." 

*Alphabet* la société mère de *Google* n'a généré que 110 milliards de dollars
de chiffre d'affaire en 2017 [^5] dont une très grande partie provient uniquement de
revenu publicitaire. Les chiffres sont moins impressionnants mais tout aussi
grand pour *Facebook*. 

Et ces revenus ne sont pas uniquement des publicités affichées lors de
l'utilisation de leurs services. Ils proviennent également de la revente de ces
données à des tiers qui vont constituer des banques de données à des fins
publicitaires mais pas uniquement.

Depuis quelques années, individuellement nous avons permis de lancer cette
tendance individuellement. Ce que je constate aujourd'hui c'est que les
entreprises le font maintenant pour nous également. Il y a  principalement la messagerie
*collaborative* qui aujourd'hui se trouve à être Gsuite ou Office365. 

On s'imagine maintenant qu'il est difficile d'avoir une adresse mail qui n'est 
pas hébergée chez un des ces gros acteurs par peur de se retrouver dans une 
RBL [^6] ou tout autre motif qui feront que l'on se retrouve dans le dossier 
*SPAM* ou dans le plus mauvais des cas, le mail n'est pas délivré. Je suis
convaincu que si tout le monde raisonne ainsi cela ne va effectivement jamais
s'arranger et la centralisation d'Internet se fera de plus en plus grande. Ou
même aura t-on plusieurs Internet? 

C'est peut-être un peu le cas quand on entend :

> "Il n'y a plus d'Internet!" 

quand seul Facebook ne fonctionne plus par exemple.

Il va falloir que j'apprenne à mieux argumenter pour défendre le sujet !






[^1]: [Article wikipedia Facebook - Affaire Cambridge Analytica](https://fr.wikipedia.org/wiki/Facebook#Affaire_Cambridge_Analytica)
[^2]: [Replay de l'émission de Laurent Delahousse](https://www.france.tv/documentaires/societe/765239-google-apple-facebook-les-nouveaux-maitres-du-monde.html)
[^3]: [Nothing to hide](https://www.kickstarter.com/projects/1587081065/nothing-to-hide-the-documentary)
[^4]: [Le film Nothing to hide sur Peertube](https://framatube.org/videos/watch/d2a5ec78-5f85-4090-8ec5-dc1102e022ea)
[^5]: [Alphabet - Wikipédia](https://en.wikipedia.org/wiki/Alphabet_Inc.)
[^6]: [Real time Black List](https://fr.wikipedia.org/wiki/Lutte_anti-spam#RBL)
