+++
author = "ValVin"
date = 2016-05-11T18:26:35Z
description = ""
draft = true
slug = "et-si-on-faisait-un-waze-libre"
title = "Et si on proposait une alternative à Waze? Libre bien entendu!"

+++

Depuis quelques temps, j'ai lancé ma dégooglisation. J'avance plutôt bien mais il me reste un élément sur lequel je n'ai pas encore réussi à trouver d'alternative : [Waze](https://www.waze.com/fr/) !

Pour ceux qui ne connaissent pas, Waze est un GPS communautaire. Il permet, en plus des signalements divers et variés dont les bonhommes bleus, de calculer le meilleur itinéraire en fonction du trafic (plus ou moins temps réel). L'éditeur de Waze est à l'origine une société israélienne, rachétée par Google il y a déjà quelques temps. D'ailleurs à son début, l'application était OpenSource et disponible notamment sur [GitHub](https://github.com/mkoloberdin/waze) mais jusqu'à la version 2.4.0, un peu avant le rachat de Waze par Google (un petit 1.15 Milliards $). Waze n'est pas tout seul, mais certainement le plus utilisé. Il y aussi TomTom, Via Michelin, Mappy, Inrix qui propose un service similaire.

Sur [Diaspora](https://framasphere.org/posts/1691458), j'avais justement lancé cette idée (qui me paraît encore utopique) de développer un service offrant une alternative à ce service respectant la vie privée (je pense que Waze a passé son tour sur ce point là) et bien entendu libre. J'avais lancé un [pad](https://mypads.framapad.org/mypads/?/mypads/group/osm-traffic-hq1v97xi/pad/view/l-idee-de-depart-lt1w979p) sur lequel quelques contributeurs ont fait avancé le schmilblik.

L'idée n'est pas de réinventer la roue et d'essayer de se baser sur un maximum de chose existantes. S'il était possible de ne pas trop  se caler sur ce qui existe et proposer quelque chose de nouveau, ce serait top! Cependant, comme je l'indique dans mon intro, l'idée est de proposer une alternative à un produit déjà très connu / utilisé donc il faudra à minima des fonctionnalités très proches.

Ci-dessous un compte-rendu de ce qui existe et peu être intéressant d'explorer pour un tel outil.

##La cartographie libre et communautaire
Peut-être que je n'ai pas effectué assez de recherche mais il me semble que LE système de cartographie libre et communautaire est sans conteste [OpenStreetMap](http://www.openstreetmap.org/#map=5/51.500/-0.100) (OSM) ! En plus de cela, la cartographie est très détaillée et permet des usages bien plus élargis que la simple navigation.

D'ailleurs c'est sur celle-ci que l'on retrouve sur l'ensemble des applications GPS libres mais également sur quelques applications non libre dont voici une liste référencée sur le [wiki d'OSM](http://wiki.openstreetmap.org/wiki/Android).

## Les applications GPS libres 
###OsmAnd~ 
OsmAnd~ est une application opensource utilisant la cartographie OSM. L'application fonctionnent aussi bien en en ligne et hors connexion.
Pour les données hors connexions, il est possible de télécharger uniquement un région particulière, ce qui évite de s'encombrer avec des cartes de plusieurs Giga octets!
L'application propose la navigation qui peut être paramétrée en fonction de son mode de transport.

L'application est très complète et propose un système de greffons sur lequel un service de routage avec prise en compte de traffic pourrait se greffer plus ou moins facilement. MAIS son défaut reste l'ergonomie qui pour une application de ce type peut être assez bloquant.

Pour ma part, j'ai utilisé la version "maintenue" par F-Droid. Une version payante est disponible sur le PlayStore mais je ne connais pas exactement la différence entre les deux versions.

####Plus d'infos
* Site Web : http://osmand.net/ 
* Repo :  https://github.com/osmandapp/Osmand
* F-Droid : https://f-droid.org/repository/browse/?fdfilter=osmand&fdid=net.osmand.plus 
* Licence : GPLv3

###Maps.me

Maps.me est également opensource et se repose OSM. La projection cartographique est en 3D (contrairement à la précédente qui est en 2D) et l'interface est très soignée et qui plus est ergonomique.

La société se trouvant derrière Maps.me édite en autres des guides touristiques. Au niveau modèle économique, il y a également une API (Android / iOS) permettant d'intéragir avec l'application Maps.Me dont une est payante.
L'application n'est pas compatible avec F-Droid et si j'en crois cet [issue](https://github.com/mapsme/omim/issues/85) c'est pas gagné... 

####Plus d'infos
* Site web :http://maps.me/en/home
* Repo : https://github.com/mapsme/omim
* Licence : Apache v2.0

###uNav

