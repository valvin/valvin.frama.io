+++
author = "ValVin"
date = 2016-04-24T06:30:23Z
description = ""
draft = true
slug = "pgrouting-calcul-dintineraire-avec-openstreetmap"
title = "pgRouting - calcul d'intinéraire avec OpenStreetMap"

+++

## Creation de la base
```
createdb osm
psql osm -c "create extension postgis"
psql osm -c "create extension pgrouting"
```

## Import avec osm2pgsql
```
osm2pgsql -s -U postgres -d osm nord-pas-de-calais-latest.osm.pbf
```

```
node cache: stored: 17780691(100.00%), storage efficiency: 56.80% (dense blocks: 10143, sparse nodes: 10459620), hit rate: 100.00%
```


## Modification de la table
```
ALTER TABLE planet_osm_roads ADD COLUMN "target" bigint;
ALTER TABLE planet_osm_roads ADD COLUMN "source" bigint;
```

## Creation de la topologie

```
SELECT pgr_createTopology('planet_osm_roads', 0.00001, 'way', 'osm_id');
```

```
NOTICE:  PROCESSING:
NOTICE:  pgr_createTopology('planet_osm_roads', 1e-05, 'way', 'osm_id', 'source', 'target', rows_where := 'true', clean := f)
NOTICE:  Performing checks, please wait .....
NOTICE:  Creating Topology, Please wait...
NOTICE:  1000 edges processed
NOTICE:  2000 edges processed
NOTICE:  3000 edges processed
NOTICE:  4000 edges processed
NOTICE:  5000 edges processed
NOTICE:  6000 edges processed
NOTICE:  7000 edges processed
NOTICE:  8000 edges processed
NOTICE:  9000 edges processed
NOTICE:  10000 edges processed
NOTICE:  11000 edges processed
NOTICE:  12000 edges processed
NOTICE:  13000 edges processed
NOTICE:  14000 edges processed
NOTICE:  15000 edges processed
NOTICE:  16000 edges processed
NOTICE:  17000 edges processed
NOTICE:  18000 edges processed
NOTICE:  19000 edges processed
NOTICE:  20000 edges processed
NOTICE:  21000 edges processed
NOTICE:  22000 edges processed
NOTICE:  23000 edges processed
NOTICE:  24000 edges processed
NOTICE:  25000 edges processed
NOTICE:  26000 edges processed
NOTICE:  27000 edges processed
NOTICE:  28000 edges processed
NOTICE:  29000 edges processed
NOTICE:  30000 edges processed
NOTICE:  31000 edges processed
NOTICE:  32000 edges processed
NOTICE:  33000 edges processed
NOTICE:  34000 edges processed
NOTICE:  35000 edges processed
NOTICE:  36000 edges processed
NOTICE:  37000 edges processed
NOTICE:  -------------> TOPOLOGY CREATED FOR  37076 edges
NOTICE:  Rows with NULL geometry or NULL id: 0
NOTICE:  Vertices table for table public.planet_osm_roads is: public.planet_osm_roads_vertices_pgr
NOTICE:  ----------------------------------------------
 pgr_createtopology 
--------------------
 OK
(1 row)
```

## Verification

```
select pgr_analyzegraph('planet_osm_roads', 0.000001,'way','osm_id');
```

```
NOTICE:  PROCESSING:
NOTICE:  pgr_analyzeGraph('planet_osm_roads',1e-06,'way','osm_id','source','target','true')
NOTICE:  Performing checks, please wait ...
NOTICE:  Analyzing for dead ends. Please wait...
NOTICE:  Analyzing for gaps. Please wait...
NOTICE:  Analyzing for isolated edges. Please wait...
NOTICE:  Analyzing for ring geometries. Please wait...
NOTICE:  Analyzing for intersections. Please wait...
NOTICE:              ANALYSIS RESULTS FOR SELECTED EDGES:
NOTICE:                    Isolated segments: 3286
NOTICE:                            Dead ends: 13250
NOTICE:  Potential gaps found near dead ends: 10379
NOTICE:               Intersections detected: 71595
NOTICE:                      Ring geometries: 2692
 pgr_analyzegraph 
------------------
 OK
```

```
CREATE TABLE tmp_route AS SELECT seq, edge, cost, agg_cost, b.way FROM pgr_dijkstra('
                SELECT osm_id AS id,
                         source,
                         target,
                         ST_Length(way) AS cost
                        FROM planet_osm_roads',
                65,72) a LEFT JOIN planet_osm_roads b ON (a.edge = b.osm_id);
```



```
ALTER TABLE planet_osm_roads ADD COLUMN x1 double precision;
ALTER TABLE planet_osm_roads ADD COLUMN y1 double precision;
ALTER TABLE planet_osm_roads ADD COLUMN x2 double precision;
ALTER TABLE planet_osm_roads ADD COLUMN y2 double precision;

UPDATE planet_osm_roads SET x1 = ST_x(ST_PointN(way, 1));
UPDATE planet_osm_roads SET y1 = ST_y(ST_PointN(way, 1));

UPDATE planet_osm_roads SET x2 = ST_x(ST_PointN(way, ST_NumPoints(way)));
UPDATE planet_osm_roads SET y2 = ST_y(ST_PointN(way, ST_NumPoints(way)));
```


```
--
--DROP FUNCTION pgr_fromAtoB(varchar, double precision, double precision,
--                           double precision, double precision);

CREATE OR REPLACE FUNCTION pgr_fromAtoB(
                IN tbl varchar,
                IN x1 double precision,
                IN y1 double precision,
                IN x2 double precision,
                IN y2 double precision,
                OUT seq integer,
                OUT gid integer,
                OUT name text,
                OUT heading double precision,
                OUT cost double precision,
                OUT geom geometry
        )
        RETURNS SETOF record AS
$BODY$
DECLARE
        sql     text;
        rec     record;
        source	integer;
        target	integer;
        point	integer;

BEGIN
	-- Find nearest node
	EXECUTE 'SELECT id::integer FROM planet_osm_roads_vertices_pgr
			ORDER BY the_geom <-> ST_GeometryFromText(''POINT('
			|| x1 || ' ' || y1 || ')'',4326) LIMIT 1' INTO rec;
	source := rec.id;

	EXECUTE 'SELECT id::integer FROM planet_osm_roads_vertices_pgr
			ORDER BY the_geom <-> ST_GeometryFromText(''POINT('
			|| x2 || ' ' || y2 || ')'',4326) LIMIT 1' INTO rec;
	target := rec.id;

	-- Shortest path query (TODO: limit extent by BBOX)
        seq := 0;
        sql := 'SELECT osm_id, way, name, cost, source, target,
				ST_Reverse(way) AS flip_geom FROM ' ||
                        'pgr_dijkstra(''SELECT osm_id as id, source, target, '
                                        || 'ST_Length(way) AS cost FROM '
                                        || quote_ident(tbl) || ''', '
                                        || source || ', ' || target
                                        || '), '
                                || quote_ident(tbl) || ' WHERE edge = osm_id ORDER BY seq';

	-- Remember start point
        point := source;

        FOR rec IN EXECUTE sql
        LOOP
		-- Flip geometry (if required)
		IF ( point != rec.source ) THEN
			rec.the_geom := rec.flip_geom;
			point := rec.source;
		ELSE
			point := rec.target;
		END IF;

		-- Calculate heading (simplified)
		EXECUTE 'SELECT degrees( ST_Azimuth(
				ST_StartPoint(''' || rec.the_geom::text || '''),
				ST_EndPoint(''' || rec.the_geom::text || ''') ) )'
			INTO heading;

		-- Return record
                seq     := seq + 1;
                gid     := rec.gid;
                name    := rec.name;
                cost    := rec.cost;
                geom    := rec.the_geom;
                RETURN NEXT;
        END LOOP;
        RETURN;
END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE STRICT;

```

```
SELECT id::integer FROM planet_osm_roads_vertices_pgr
    ORDER BY the_geom <-> ST_GeometryFromText('POINT(50.6910, 2.8825)',4326);
```