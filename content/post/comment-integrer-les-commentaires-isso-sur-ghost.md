+++
author = "ValVin"
categories = ["blog"]
date = 2015-10-09T07:17:04Z
description = ""
draft = false
featured_image = "/content/images/2015/10/balloons-874838_640.jpg"
slug = "comment-integrer-les-commentaires-isso-sur-ghost"
tags = ["blog"]
title = "Isso - un gestionnaire de commentaire idéal pour Ghost"

+++

##Isso - un gestionnaire de commentaire idéal pour Ghost
Ghost ne fournit malheuresement pas par défaut de gestionnaire de commentaires. En faisant quelques recherches, on retrouve facilement des tutoriels qui proposent d'intégrer Disqus.Je ne suis pas un grand connaisseur de cette plateforme, cependant, je ne comprends pas l'intérêt d'avoir les commentaires de mon blog hébergés chez un tiers. De plus il est nécessaire d'avoir un compte Discus pour laisser un message. Il y a des alternatives un peu plus ouvertes comme Discourse mais le principe reste le même pour moi.

J'ai eu un peu de mal à trouver un système qui permettrait de proposer des commentaires sans authentification et avec modération. Mais j'ai finalement trouvé **[Isso](http://posativ.org/isso/)**

###Isso c'est quoi exactement
Isso est un serveur (léger) de commentaires écrit en **Python** et Javascript. Il stocke ces données dans une base de données **SQLLite** et propose le formatage **Markdown** dans les commentaires.

Le stockage de données SQLLite et le formatage Markdown sur Ghost, ça a eu du sens :)

Concrètement ça ressemble à ça :
![Commentaires Isso](/content/images/2015/10/Capture-du-2015-10-09-10-23-24.png)

Bien entendu, il y a les notifications par mails et la possibilité de modérer directement les commentaires depuis celui-ci.

Au niveau du serveur, on protection contre le SPAM est également présente.

###Comment ça se met en place ?
J'avoue que j'ai un peu tatonné mais avec un peu de recul, ce n'est pas lié à la documentation qui est complète, mais plutôt à l'interface chaise-clavier.

J'ai suivi la documentation d'installation : [ici](http://posativ.org/isso/docs/install/)
####Installation du serveur
Pour l'installation j'ai choisi pip :
`pip install isso`

J'ai utilisé le fichier de configuration exemple qui est assez complet : [ici](https://github.com/posativ/isso/blob/master/share/isso.conf)

voici les parties que j'ai utilisée :
```
[general]
dbpath = /path/to/comments.db
host =
    https://blog.domain.tld
notify = smtp
[moderation]
enabled = true
purge-after = 30d
[server]
listen = http://localhost:8081
[smtp]
username = smtpuser
password = password
host = mail.server.tld
port = 587
security = starttls
from = "BLOG comment" <smtpuser@server.tld>
timeout = 20
```
Quelques petites remarques : 

* si le site utilise https, il faut que le certificat soit valide
* si votre messagerie est un peu longue à la détente, il faut augementer le tiemout de `10`s à `20`s
* si votre message utilise ssl `security = ssl` et `port = 465`

un fois ce fichier créé dans `/etc/isso.conf`, pour voir si ça fonctionne :
```
isso run
```
Le résultat devrait être :

```
2015-10-09 10:50:39,155 INFO: connected to SMTP server
2015-10-09 10:50:39,239 INFO: connected to https://blog.valvin.fr
```
Il y a des petits warning sur le hash et le salt que je n'ai pas encore investigué.

####Configuration Nginx
J'ai fait simple, sur la configuration de mon Blog, j'ai juste rajouté cette section :

**EDIT 10/10/15 :**

~~
location /comments/ {
                proxy_pass http://127.0.0.1:8081/;
        }
~~
~~Ce n'est pas tout à fait ce qui est indiqué dans la doc d'installation mais ça fonctionne :)~~

Il faut toujours suivre la doc ! Sans les headers précisés, les liens de modération présent dans les mails ne contiennent pas l'uri du serveur mais `http://127.0.0.1:xxxx/`

EDIT 12/10/15 : décidemment, je ne lis pas bien la doc, alors qu'elle est très complète. Il me manquait `proxy_set_header X-Script-Name /comments;` dans ma conf Nginx. Pourtant c'est bien expliqué [ici](http://posativ.org/isso/docs/setup/sub-uri/) (RTFM)

Il faut donc utiliser cette configuration ci :
```
location /comments/ {
                proxy_pass http://127.0.0.1:8081/;
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header X-Script-Name /comments;
                proxy_set_header Host $host;
                proxy_set_header X-Forwarded-Proto $scheme;
        }
```
####Intégration côté client
Je n'ai mis les commentaires que sur le billet lui-même. 
Au niveau de ghost, cela se situe dans `/path/to/ghost/content/themes/mytheme/post.hbs`
Dans la balise `<article>` juste à avant la fermeture de la balise `</footer>`, j'ai juste ajouté le code suivant :
```
<section>
<script data-isso="/comments/"
        src="/comments/js/embed.min.js"></script>

<section id="isso-thread"></section>
</section>
```

####Lancement depuis systemd
J'ai testé avec **Gunicorn** dont le fichier service est [ici](https://github.com/jgraichen/debian-isso/blob/master/debian/isso.service) mais j'ai eu du souci avec l'ouverture de la socket ... 
J'ai donc utilisé ce fichier ci qui doit être améliorer pour le redémarrage automatique et les logs:

`/etc/systemd/system/isso.service` :
```
[Unit]
Description=lightweight Disqus alternative

[Service]
User=isso
Environment="ISSO_SETTINGS=/etc/isso.conf"
ExecStart=/usr/local/bin/isso run

[Install]
WantedBy=multi-user.target

```
Ensuite il faut activer le service :
```
systemctl enable isso
```
Puis le démarrer :
```
systemctl start isso
```
