---
author: "Valvin"
title: "Feneas - une association qui promeut la Fediverse"
featured_image: "/content/images/2019/logo-feneas.jpg"
tags: ["fediverse", "décentralisation", "opensource"]
date: 2019-02-20T20:48:30+01:00
draft: false
---

[Feneas.org](https://feneas.org/) - *The Federation Network*  est une association dont l'objectif est de promouvoir les projets dont le principe de fonctionnement est décentralisé. L'association a été co-fondée en Août 2018 par **Jason Robinson** [^1] et **Lukas Matt** [^8]. 
Association que j'ai rejoint en tant que membre en ce debut d'année.

## Qu'est ce qu'un projet décentralisé, fédéré ?

### Centralisé vs Décentralisé

Je vais prendre le contre exemple, beaucoup de personne utilise des plateformes comme Facebook, Instagram, Twitter ... Pour pouvoir utiliser, communiquer sur les services proposés, il est nécessaire de créer un compte sur cette plateforme et les services proposés ne seront disponibles que sur cette plateforme et ne peuvent pas échanger avec les autres plateformes. Ces réseaux reposent sur un fonctionnement centralisé. Par exemple :

* je ne peux pas répondre à un message Facebook avec mon compte Twitter.
* je ne peux pas suivre un compte Instagram depuis Twitter
* la decision de la légitimité d'un message est réalisée par les politiques, decisions d'une seule entité

On parle alors de centralisation, la totalité de ce qui est disponible sur la plateforme n'est gérée que par une seule entité.

Les services centralisés sont un peu different de ce que l'on imaginait à l'origine pour Internet. Car celui-ci se veut être un réseau robuste grâce au fait qu'il n'y ait pas de point central. Il a été pensé comme un réseau décentralisé.

Un des composants d'Internet est le **DNS** [^2]. C'est lui qui permet de trouver le serveur ou se trouve mon blog. Il traduit blog.valvin.fr en une adresse IP. Ce service est décentralisé car il y a des organismes differents qui gèrent le "domaine de tête" par pays (`.fr`, `.be` …) tout comme c'est un serveur différent qui gère le domaine `.valvin.fr` et je pourrais tres bien choisir d'avoir un autre serveur qui gère le domaine `.new.valvin.fr`. Le DNS est donc un service décentralisé même s'il y a une notion de hiérarchie dans les responsabilité. C'est un organisme qui décide des nouvelles extensions comme le `.dev` mais dont la gestion peut être une entreprise comme Google.

L'autre service *historique* décentralisé est la **messagerie**. Meme si aujourd'hui c'est malheuruesement de moins en moins le cas avec des géants comme GMail.com, Outlook.com, … En raison de leur omnipresence, dans certaines situation, il peut etre difficile d'envoyer un mail à ceux-ci car on est présumé coupable (de spam). Cependant, cela reste possible, par exemple, j'héberge sur un de mes serveurs ma messagerie sur mon nom de domaine. Je detiens donc l'ensemble des données que sont mes mails, ils ne sont pas sur le serveur de quelqu'un d'autre.

### Fédéré

Il y aussi la fédération que je definis comme etant le mécanisme qui permet à plusieurs serveurs (ou instances) de communiquer ensemble afin de constituer un seul et même reseau.

Si on prend l'exemple de **Mastodon** [^3] dont les fonctionnalités sont proches de Twitter, on peut suivre et communiquer avec un utilisateur qui se trouve sur une autre instance (serveur) de façon totalement transparente grâce a la fédération qui repose sur le protocole ActivityPub. Et même, un utilisateur Mastodon peut communiquer avec d'autres reseaux fédérés. Certains utilisent également **ActivityPub** [^4] mais aussi d'autres protocoles comme StatusNet utilisé par GNUsocial. On obtient donc un gigantesque réseau constitué de plusieurs reseaux fédérés. On l'appelle aussi **Fediverse** [^5] [^6]

Tout ceci pour dire que l'association **Feneas** a pour but de promouvoir l'ensemble de la Fediverse [^7]. La décentralisation d'Internet  étant une de mes convictions fortes, la Fediverse fait donc partie de mes centre d'intérêt. Je suis très heureux d'avoir rejoint l'association et j'espère pouvoir contribuer à celle-ci du mieux que je le pourrais.

[^1]: [Site personnel de Jason Robinson](https://jasonrobinson.me/)
[^2]: [Vidéo de présentation du DNS par l'AFNIC](https://www.youtube.com/watch?v=dcIrB8qRCbA)
[^3]: [Join Mastodon](https://joinmastodon.org)
[^4]: [Wikipédia - ActivityPub](https://fr.wikipedia.org/wiki/ActivityPub)
[^5]: [Wikipédia - Fediverse](https://fr.wikipedia.org/wiki/Fediverse)
[^6]: [The federation info](https://the-federation.info/)
[^7]: [Feneas - les misisons](https://feneas.org/mission/)
[^8]: [Site personnel de Lukas Matt](https://matt.wf/)
