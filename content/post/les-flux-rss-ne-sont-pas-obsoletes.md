---
title: "Les flux RSS ne sont pas obsolètes"
date: 2021-04-04T14:00:15+02:00
tags: ["rss","gafam"]
draft: false
---

Il y a déjà quelques temps que les formats *rss* et *atom* existent. On parle
de flux de syndication. Mais avec l'arrivée des grandes plateformes, ils ont
été un peu délaissé mais faut-il pour autant les considérer comme obsolètes?

La réponse est de mon point de vue évidente mais hélàs est-ce la même que vous?

Pour moi c'est un grand OUUUUIIII!!!

Revenons-en au basique, de quoi s'agit-il? Les deux formats reposent sur *XML*
et permettent de décrire les entrées de contenus divers et variés. Ce peut être
un nouvel article de blog, un nouveau contenu dans un wiki, un commentaire, une
nouvelle version de notre logiciel préféré, une nouvelle vidéo, un nouveau
message sur un réseau social... La liste est longue et extensible à souhait.

Ce flux est donc mis à jour automatiquement dès qu'un nouveau contenu est créé.

Pour en bénéficier, on utilise un lecteur de flux RSS. Un nom peut être connu
par plus de monde est *feedly* qui repose principalemet sur une offre payante
et n'est malheureusement pas Libre. Mais avant lui il y avait *Google reader*
qui depuis quelques années a fermé ses portes. Mais bien entendu il y a de
l'offre dans le monde du Libre comme *Tiny Tiny RSS* qui est celui que
j'utilise mais aussi *FreshRSS*. Il en existe d'autres mais l'objectif n'est
pas le lister tous ici.

Alors pourquoi a t-on petit à petit délaisser ce format ces dernières années?
Je pense que cela est dû à l'arrivée des plateformes comme Twitter, Facebook...
Moi le premier j'utilisais twitter pour suivre les comptes des sites dont
j'avais l'habitude d'aller et j'ai commencé à ne plus utiliser mon lecteur de
flux qui a l'époque était *Google reader*. Ensuite, c'est je pense Facebook qui
a enfoncé le clou. Petit a petit on ne publiait plus sur son site, blog mais sur
Facebook. Pour le grand public c'était clairement une nouvelle façon simple de
s'exprimer et "tout le monde" était là et ceux qui n'y était pas n'avait rien
compris. On a donc commencer à cesser de produire des contenus en dehors de ce
type de réseau social. Puis cela s'est généraliser aux entreprises, commerces
qui utilisent principalement ces médias pour communiquer. Sauf qu'en utilisant
uniquement ces plateformes, dans un temps relativement rapide, beaucoup se sont
enfermés (involontairement) soit pour intéragir avec leurs publics soit pour
suivre le contenu qui l'interesse. Et ce format de "numérisation" était bien
plus abordable que de mettre la vitrine de son entreprise / commerce en ligne.

Si c'est simple et facile, pourquoi ce n'est pas bien? Parce que le contenu
n'est pas affiché à qui souhaite le lire mais par un algorithme dont l'intérêt
et de faire en sorte que vous restiez le plus longtemps sur la plateforme afin
que vous visualisiez le plus de publicité. C'est ce qui mène à ce que dans
votre fil, vous ne voyez que des informations dont l'algorithme pense
correspondre à votre opinion. On parle de "bulle", les lectures de contenus qui
ne vont pas votre sens disparaisse petit à petit. L'intérêt de la plateforme
est de rentabiliser le contenu lui même mais aussi le visiteur. En plus il est
de plus en plus difficile d'accéder à ce contenu si on ne dispose pas de compte
sur ces plateformes. Regardez *youtube* même si on peut toujours accéder au
contenu sans compte à quel point ils insistent pour que vous vous connectiez
avec votre compte Google. Je n'aborde pas aussi la forme du contenu dont
l'objectif et de déclencher une intéraction ("j'aime", repartage...) car sinon
"ce n'est pas un bon contenu".

Je divague un peu, revenons au format *rss* / *atom*. En utilisant un lecteur
de flux, vous pouvez trier ces différentes sources d'informations comme vous le
souhaitez et choses importantes elles s'affichent au fil des publications. Le
lecteur de flux bien souvent est un lieu où on ne tente pas de rentabiliser le
contenu ou sa lecture. Dans le principe, vous visualisez le titre et la
description du contenu dans votre lecteur et vous terminez la lecture sur le
site original pour l'article complet. Dans certains cas, on peut tout lire
directement dans le lecteur de flux ce qui peut être intéressant.

Mais voilà, les producteurs de contenus ne mettent plus en avant ce format, les
plateformes n'ont aucun intérêt à le mettre en avant non plus puisqu'elles
perdent le contrôle. 

Et donc, on enterre ces formats? Non et heureusement. Ces formats sont bien
souvent disponibles mais il faut les chercher. Sur une très grande partie des
solutions de blogs, de sites statiques ou tout autre générateur de contenus
c'est présent par défaut. Sauf qu'il faut le chercher pour le trouver et c'est
bien dommage. Pour les plateformes, il y a des solutions comme *RSSBridge* qui
permet de générer un flux à partir des publications générées sur différentes
d'entres-elles. On peut donc "tricher" pour sortir des plateformes et
bénéficier d'une information sélectionné par vous et non pour vous.
Malheureusement ces plateformes vérouillent de plus en plus et pendant
l'écriture de cet article, Facebook ne permet qu'aléatoirement d'accéder à des
pages publiques sans authentification.

Autre point important pour le producteur de contenu, sans pour autant vous
éloignez de votre audience, si vous le pouvez, créer le contenu indépendament
de la plateforme et publiez sur les différentes plateformes un lien vers ce
contenu. Alors c'est bien plus facile à dire qu'à faire mais il existe des
solutions, ne mettez pas tout vos oeufs dans le même panier. Car tout en se
concentrant sur l'audience de telle ou telle plateforme, le contenu est
*vérouillé* sur celle-ci. Et indirectement vous vous éloignez d'une audience en
dehors de cette plateforme.

Malheureusement tout n'est pas rose. Le fait de devoir chercher ou créer le
flux, de ne pas publier au plus vite, au plus simple. Mais je suis convaincu
que tout n'est que positif que de reprendre un peu le contrôle. 

Petit à petit les plateformes sont ce que trop de personnes appellent
"Internet". Mais pensez-vous normal que ceux sont ces plateformes qui
choisissent quel contenu vous est préférable de lire? Pensez-vous que tout
contenu que vous produisez doit leur appartenir et en empêche l'accès à des
personnes qui n'ont pas créer un compte sur leur plateforme? 

**REPRENEZ LE CONTRÔLE !!! :)**

## Quelques liens

* [Format RSS](https://fr.wikipedia.org/wiki/RSS)
* [Format Atom](https://fr.wikipedia.org/wiki/Atom_Syndication_Format)
* [Tiny Tiny RSS](https://tt-rss.org/) - pour ceux que ça intéresse je peux ouvrir un compte sur mon instance
* [FreshRSS](https://www.freshrss.org/)
* [RSS Bridge](https://github.com/RSS-Bridge/rss-bridge)
