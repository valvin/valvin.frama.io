+++
author = "ValVin"
categories = ["dns", "smtp"]
date = 2015-10-14T10:16:32Z
description = "Cet article présente deux enregistrements DNS pour éviter que votre messagerie soit considérée comme du SPAM : SPF et PTR "
draft = false
featured_image = "/content/images/2015/10/spam-964521_1280.jpg"
slug = "messagerie-dns-spf-ptr"
tags = ["dns", "smtp"]
title = "Messagerie - DNS - SPF et PTR"

+++

Dans l'[article précédent](https://blog.valvin.fr/2015/10/12/messagerie-dns-mx/), nous avons parcouru rapidement le DNS et les différentes types d'entrées et notamment l'enregistrement du (des) MX. Même si techniquement, cet enregistrement est suffisant pour le bon fonctionnement du protocole SMTP, il ne le sera pas pour que la messagerie ne soit pas considérer comme du SPAM. L'enregistrement **SPF** et le **Reverse DNS** sont deux enregistrements permettant d'éviter cela, auxquels s'ajouteront le DKIM et DMARC.  

##L'enregistrement SPF
[Sender Policy Framework](https://en.wikipedia.org/wiki/Sender_Policy_Framework) est un enregistrement permettant de valider si le serveur SMTP qui utilise le domaine est autorisé à le faire. Cela a été mis en place afin de réduire le SPAM.

Concrètement, si je reçois un email provenant de contact@tf1.fr, la messagerie fait une requête de ce type :
```
nslookup -type=TXT tf1.fr
```

```
tf1.fr	text = "MS=ms41345307"
tf1.fr	text = "adobe-idp-site-verification=2759e3a4-b2b4-4ea0-87dd-6a83dde0d0a8"
tf1.fr	text = "v=spf1 ip4:193.164.156.0/24 ip4:185.22.116.0/22 include:spf.protection.outlook.com -all"
```
La requête retourne plusieurs enregistrements TXT, celui qui nous intéresse et qui indique que l'enregistrement est du SPF est celui qui commence par `v=spf1`. Le '1' correspond à la version de SPF utilisée. Ensuite, on indique quels sont les serveurs qui ont l'autorisation d'envoyer des emails. 

Dans notre exemple, le champs SPF indique des sous-réseaux IPv4 en notation [CIDR](https://en.wikipedia.org/wiki/Classless_Inter-Domain_Routing#CIDR_notation) : `ip4:193.164.156.0/24` et `ip4:185.22.116.0/22`. Cela signifie que toutes les adresses appartenant à ces deux sous-réseaux sont autorisées à envoyer des mails avec le domaine `tf1.fr`. Avec le champs `ip4`, il est possible de mettre une adresse IP plutôt qu'un sous-réseau : `ip4:193.164.156.1` par exemple.
On peut étendre ce fonctionnement avec le champs `ip6` qui permettra de mettre des adresses et sous réseaux IPv6.

L'exemple, ajoute un champs `include:spf.protection.outlook.com`, ce qui permet d'étendre la liste des serveurs avec ceux précisés dans l'enregistrement `spf.protection.outlook.com`
```
spf.protection.outlook.com	text = "v=spf1 ip4:207.46.101.128/26 ip4:207.46.108.0/25 ip4:207.46.100.0/24 ip4:207.46.163.0/24 ip4:65.55.169.0/24 ip4:157.55.133.0/25 ip4:157.56.110.0/23 ip4:157.55.234.0/24 ip4:213.199.154.0/24 ip4:213.199.180.0/24 include:spfa.protection.outlook.com -all"
```
et ainsi de suite...

Le champs `-all`, indique qu'aucun autre serveur n'est autorisé. Le champs `all` est souvent préfixé par l'opérateur `-` ou `~`. 

Ces opérateurs indiquent le résultat de la vérification SPF et s'applique a tous les champs utilisés dans SPF.

* `+` pour un résultat *PASS*. Il peut être omit
* `?` pour un résultat *NEUTRAL* interprété comme *NONE* (aucune règle)
* `~` pour un résultat *SOFTFAIL*, un aide à l'évalutation entre NEUTRAL et FAIL. Typiquement, les messages qui retournent un SOFTFAIL sont acceptés mais marqués.
* `-` (minus) pour un résultat *FAIL*, le mail sera refusé

Non présent dans l'exemple mais très utile pour une messagerie personnelle :

* Le champs `mx`, qui indique que tous les enregistrements MX sont autorisés. Ce qui, dans la majorité des cas, répond au besoin.
* Le champs `a`, qui lorsqu'il n'ait pas suivi d'un nom d'hôte, indique que si un enregistrement A ou AAAA existe sur domain.tld, il sera autorisé également.

Dans certains cas, il est également utilisé le champs `redirect`, celui-ci permet de faire un lien vers un enregistrement SPF d'un autre domaine. Il se note `redirect:domain2.tld`

###Exemple d'enregistrement SPF pour sa messagerie

Un enregistrement SPF qui pourra fonctionner dans de nombreux cas pour une messagerie personnelle:
```
@ 10800 IN TXT "v=spf1 a mx -all"
```

Celui-ci autorise les enregistrements MX, est les enregistrement A et AAAA de domain.tld a envoyé des mails et interdit toutes les autres serveurs.

##L'entrée PTR pour le serveur SMTP
En complément au SPF, il y a un enregistrement à mettre à jour sur lequel vous n'avez pas directement la main. Il s'agit du **Reverse DNS**  de votre (vos) serveur(s) SMTP.

Nous avons l'enregistrement de type A qui pour un nom d'hôte fournit son adresse IPv4. Le Reverse DNS, c'est le strict opposé : pour une adresse IPv4, le DNS retourne le nom d'hôte. Il s'agit d'un enregistrement de **type PTR**

Si notre serveur SMTP s'appelle `mx.domain.tld` pour lequel nous avons un adresse IP `W.X.Y.Z`, alors **chez l'hébergeur ou votre FAI** existe une entrée de type PTR qui porte le nom `Z.Y.X.W.in-addr.arpa` (l'adresse IP est écrite de droite à gauche). Il faut lui faire attribuer la valeur `mx.domain.tld`

Pour cela, il n'y a qu'à demander et laisser faire votre hébergeur ou FAI.

En conlusion, le champs SPF permet de valider les serveurs pouvant envoyer des mails. Le Reverse DNS permet de vérifier que l'adresse IP et le nom d'hôte corresponde bien. Cependant, il reste encore des enregistrements supplémentaires comme DKIM et DMARC afin d'avoir une messagerie opérationnelle d'un point de vue DNS.


