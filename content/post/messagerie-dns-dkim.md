+++
author = "ValVin"
categories = ["dns", "smtp"]
date = 2015-10-21T13:33:10Z
description = ""
draft = false
featured_image = "/content/images/2015/10/binding-contract-948442_1280.jpg"
slug = "messagerie-dns-dkim"
tags = ["dns", "smtp"]
title = "Messagerie - DNS - DKIM"

+++

Dans les articles précédents, nous avons vu les enregistrements DNS [MX pour déclarer ces serveurs SMTP](https://blog.valvin.fr/2015/10/12/messagerie-dns-mx/), [SPF pour autoriser les serveurs](https://blog.valvin.fr/2015/10/14/messagerie-dns-spf-ptr/) ayant le droit d'utiliser le domaine pour expédier des emails. Cet article a pour but de présenter le **protocole DKIM** qui va utiliser également un enregistrement DNS  mais ce n'est qu'un maillon de la chaine. 
Je ne traiterais pas dans cet article la configuration DKIM côté messagerie, mais peut être dans un prochain article :)

##DomainKeys Identified Mail - DKIM
Tout comme l'enregistrement [SPF](https://blog.valvin.fr/2015/10/14/messagerie-dns-spf-ptr/), [DKIM](https://en.wikipedia.org/wiki/DomainKeys_Identified_Mail) - [RFC4871](https://tools.ietf.org/html/rfc4871) a pour objectif d'authentifier le serveur afin de **limiter le SPAM**. Sauf que cette authentification est un peu plus forte et utilisera un chiffrement à base d'une clé publique et d'une clé privée. 
![](/content/images/2015/10/berlin-key-96225_640-resized.jpg)
Le [chiffrement asymétrique](https://fr.wikipedia.org/wiki/Cryptographie_asym%C3%A9trique) propose soit de **rendre confidentiel un message** en le chiffrant avec la clé publique du destinataire que seul lui pourra lire grâce à sa clé privée. Soit de **signer un message** avec sa clé privée afin d'en assurer son **intégrité**, le destinataire utilisera la clé publique de l'émetteur pour vérifier que le contenu du message est valide. 
Dans DKIM, on utilise **la signature** afin de s'assurer que c'est effectivement le bon serveur qui envoie le message. C'est un peu comme la signature [PGP](https://fr.wikipedia.org/wiki/Pretty_Good_Privacy) que l'on peut intégrer dans nos emails. Afin d'avoir une sécurité correcte, il ne faudra pas utilisé un clé inférieure à 1024 bits.

DKIM indique un entête dans le mail dont voici les champs principaux : 

* `v=1`, la version utilisée 
* `a=rsa-sha256`, le hash utilisé pour la signature
* `d=domain.tld`, le domaine de messagerie 
* `s=selector`, le sélecteur DKIM qui avec le domaine de messagerie permet d'interroger le DNS sur `selector._domainkey.domain.tld` et d'obtenir la clé publique DKIM correspondante. Il s'agit de l'enregistrement DNS qui nous intéresse
* `h=xxx`, la liste les champs de l'entête qui seront signés, par exemple : `Date:From:To:Subject:MIME-Version:Content-Type:List-Unsubscribe:Message-ID`
* `bh=xxx` est le hash du corps du message
* `b=xxx` est la signature DKIM et sera la valeur vérifiée par la messagerie à réception du message

Voici un exemple d'entête DKIM reçu de la part de Twitter :
![](/content/images/2015/10/Capture-du-2015-10-21-15-14-17.png)

La messagerie à réception de ce message va donc récupérer la clé publique déclarée dans le DNS par l'enregistrement `dkim-201406._domainkey.twitter.com` et vérifier la signature présente dans le champs `b=` qui n'a pu être générée que par un serveur disposant de la clé privée correspondante. 

Je ne vais pas plus loin dans la mise en place de DKIM car l'article traite de l'enregistrement DNS lui même. Petit indice, l'implémentation de DKIM peut se faire facilement avec [OpenDKIM](http://www.opendkim.org/)  sur Postfix. De mon côté, j'ai profité de l'implémentation "toute faite" de Synology.

##L'enregistrement DNS DKIM
L'enregistrement DNS utilisé dans le protocole DKIM permettra de fournir la clé publique de notre serveur. Cela permettra aux autres serveurs SMTP de vérifier la signature incluse dans le message.
Comme vu précédemment, l'enregistrement DNS sera composé d'un sélecteur, suivi de `_domainkey.domain.tld`. Ce selecteur devra correspondre à celui paramétré dans votre messagerie (opendkim).
L'enregistrement DNS DKIM est donc `selector._domainkey.tld`.
 
Si je reprends l'exemple plus haut, l'enregistrement DNS DKIM est `dkim-201406._domainkey.twitter.com`. 
On obtient donc le certificat public de Twitter en faisant la commande suivante :
`nslookup -type=TXT dkim-201406._domainkey.twitter.com`
```
Non-authoritative answer:
dkim-201406._domainkey.twitter.com	text = "v=DKIM1\; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwe34ubzrMzM9sT0XVkcc3UXd7W+EHCyHoqn70l2AxXox52lAZzH/UnKwAoO+5qsuP7T9QOifIJ9ddNH9lEQ95Y/GdHBsPLGdgSJIs95mXNxscD6MSyejpenMGL9TPQAcxfqY5xPViZ+1wA1qcr"
```

L'enregistrement DNS DKIM est donc un champs de type `TXT` qui contiendra les informations suivantes :

* `v=` qui permet de spécifier la version DKIM utilisée, aujourd'hui nous utilisons la version 1 ce qui donne `v=DKIM1`. J'avoue ne pas connaitre la raison exacte du caractère `\` dans l'exemple mais celui est présent dans de nombreux enregistrement. Si vous avez l'explication, je suis preneur.
* `k=rsa` (optionnel car c'est la valeur par défaut) qui indique que l'on va utiliser RSA pour générer et vérifier la signature. C'est la raison pour laquelle celle-ci n'est pas présente dans l'enregistrement de Twitter
* `p="xxxx"` qui indique le certificat public à utiliser pour vérifier les signatures.

On obtiendra donc un enregistrement de ce type si on choisit comme sélecteur DKIM `mail` :
```
mail._domainkey 10800 IN TXT "v=DKIM1; k=rsa; p=MONCERTIFICATPUBLICENBASE64"
```
Quelques exemples :
```
nslookup -type=TXT proddkim1024._domainkey.linkedin.com
```
```
Non-authoritative answer:
proddkim1024._domainkey.linkedin.com	text = "v=DKIM1\;  p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDnp6VIffGakD19jp9mLNI4+Qn5Rx/mqBYOJOp+oMEUxmYRJB5+dtow7EZ2VRJQdIHwRQlYJ7+DJYX6DVvRJb7yfP119kueITuq9CfwPloMEb0Ds/TCItQNNXRBTIlZCE94p0qyEbS0x1pJA1MGaEiBhNrH5mNPRgybeu+JfnoNtQIDAQAB\;"

```
```
nslookup -type=TXT mailout._domainkey.ovh.com
```
```
Non-authoritative answer:
mailout._domainkey.ovh.com	text = "v=DKIM1\; k=rsa\; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDLRNQLXoWaKmGbUV2IUmI0JRNXMJ76VyMWnQdIRX0mC9mS7aHFG/iR2hCj5w4NPpRJTeA5t9TYkd15bgY7HK4KddGhwOQ+oj6YJdFjF/ODVIsNOkoZQC8KWdlufXRf3pyYwWMZzUUNRXxs+CbL1m0m2XgZ84Ka5dPtuzFSBT6jYQIDAQAB"
```

####Sources

* Wikipédia : [DomainKeys Identified Mail](https://en.wikipedia.org/wiki/DomainKeys_Identified_Mail) 
* IETF : [RFC4871](https://tools.ietf.org/html/rfc4871)
* Pixabay :
 * [Berlin key](https://pixabay.com/fr/cl%C3%A9-de-berlin-cl%C3%A9-fermer-s%C3%BBr-96225/)
 * [Binding Contract](https://pixabay.com/fr/contrat-liant-contrat-secure-accord-948442/)