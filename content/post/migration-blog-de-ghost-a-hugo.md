+++
author = "Valvin"
date = 2017-11-14T21:30:25+01:00
title = "Migration du blog de Ghost a Hugo avec GitLab Pages"
featured_image = "/content/images/2017/framagit_pipeline.png"
description = ""
draft = false
tags = ["blog","hugo"]

+++

## Au revoir Ghost

Tout récemment j'ai migré mon blog de [Ghost](https://ghost.org) qui héberge celui-ci depuis un peu plus de deux ans. La raison de la migration est toute simple (voir trop) : 

Les nouvelles versions de Ghosts ne fonctionnent plus avec une base de données SQLlite en environnement de production. Il force à utiliser une base de données MySQL que je n'envisageais / voulais / pouvais pas installer sur mon VPS. J'avoue que je n'ai pas cherché très longtemps et j'en ai fait une occasion pour tester d'autres solutions.

Ce qui ne fait pas de Ghost un mauvais produit. D'autant que les dernières versions se sont bien améliorer au niveau outillage, notamment pour les mises à jour. 

![](/content/images/2017/ghost_logo.png)

Celui-ci m'a permis de me lancer (modestement), je l'avais choisi pour deux raisons principales : 

* Éditeur Markdown qui en plus disposait d'un upload assez facile des images 
* NodeJS car c'était le petit truc du moment et que je m'y intéressais

Il manquait un moteur de gestion de commentaires que j'ai pu vite combler avec Isso. 

Merci Ghost pour ces deux années passées ensemble et les quelques posts que j'ai eu / pris le temps d'écrire.

## Les génrateurs de sites statiques

En faisant mes recherches dont le cahier des charges était relativement simple :

* Base de données fichier (SQLLite par exemple)
* Éditeur Markdown
* Gestionnaire de commentaire intégré

Je me suis rendu compte que j'allais vite faire le tour de ce qui était possible. J'ai commencé à étudier PluXML qui n'avait pas d'éditeur Markdown mais avait l'avantage de tout stocker sous forme de fichier (XML). Mais bien que l'outils semble assez puissant et dispose de nombreuses extensions, je n'ai pas trop apprécié l'éditeur. Je dois avouer que le Markdown m'a vite manqué.

Je me suis donc retrouvé face aux générateurs de sites statiques ... et je me suis dit pourquoi pas !

J'ai commencé par étudier Jekyll qui est très simple à tester. Mais, effet de mode ou pas, je me suis plus laissé tenter par Hugo. Pourtant, j'ai eu un peu de mal à installer le client mais cela fera l'objet d'un autre post.

![](/content/images/2017/gohugo_io.png)

Le gros avantage des générateurs de sites statiques c'est que tout est sous la forme d'HTML, CSS et JS. Tout est executé côté navigateur, il n'y a pas de serveur d'application! Il suffit juste trouver le moyen d'héberger ces pages sur un serveur web le plus élémentaires qu'il soit.  

## Les GitLab Pages

Et justement, comme j'avais envie de creuser ce que l'on pouvait faire avec GitLab, je me suis intéressé aux [GitLab Pages](https://about.gitlab.com/features/pages/) qui est la fonctionnalité récemment proposée dans la version communautaire. C'est clairement un équivalent des Github Pages.

![](/content/images/2017/gitlab_pages_logo.png)

Cette fonctionnalité est compatible avec de nombreux générateurs de contenus. La liste complète proposée par GitLab est [ici](https://gitlab.com/groups/pages?non_archived=true&sort=latest_activity_desc)

Le tout est couplé avec les "Runners" de GitLab. Il s'agit de la partie intégration continue de GitLab. J'avoue qu'avant de regarder ce sujet, je ne connaissais pas spécialement. Concrètement, il est possible de déclencher des actions en fonction de certains critères. Cette action va se dérouler dans un containeur Docker, réaliser certaines actions et fournir un résultat. Dans notre cas, il va générer dans le répertoire `public` contenant l'ensemble de notre site. C'est ce que l'on retrouve dans le fichier `.gitlab-ci.yml`. 

```yml

variables:
  GIT_SUBMODULE_STRATEGY: normal

image: registry.gitlab.com/pages/hugo:latest

test:
  script:
  - hugo
  except:
  - master

pages:
  script:
  - hugo
  artifacts:
    paths:
    - public
  only:
  - master
```


Dans celui-ci, on dit "grosse-maille" que dès que l'on pousse un nouveau changement sur la branche "master", on instancie un nouveau containeur docker, ici celui d'Hugo, et on lance la commande hugo qui générera automatiquent notre site à partir de notre dépôt sur lequel on est en train de travailler.

La variable `GIT_SUBMODULE_STRATEGY` permet de synchroniser les sous-modules git attaché à mon dépot. En l'occurence, il s'agit ici de mon thème que j'ai récupéré d'un dépôt existant et que j'essaie de mettre à jour en *tentant* de repsecter le travail qui avait été déjà réalisé.

Dans le cas ou tout se passe bien, on retrouve son site sur https://username.xxxx.yy/projectname.

Dans mon cas, je profite des services de Framasoft avec [Framagit](https://framagit.org), on obtient donc une URL du type `https://user_name.frama.io/project_name`

Pour que le site soit à la racine, il suffit juste de changer le nom du projet en `user_name.frama.io`

On retrouve donc ce blog sur le dépôt que se trouve [ici](https://framagit.org/valvin/valvin.frama.io) et le thème [Ananke](https://themes.gohugo.io/gohugo-theme-ananke/) se trouve [ici](https://framagit.org/valvin/gohugo-theme-ananke) avec toutes mes modifications, notamment l'ajout d'Isso.


Il y a un petit côté magique dans génration automatique à chaque `git push` ! En plus, d'un point de vue collaboratif, on peut profiter de la puissance de Git et GitLab (Merge Request notamment).

## La gestion de commentaires

Malheureusement, avec les sites statiques, il n'y a pas de possibilités de gérer les commentaires. J'ai donc fait un peu le fénéant et j'ai repris ce que j'utilisais précédemment avec Ghost : [Isso](https://posativ.org/isso/).

J'avais expliqué à l'époque comment l'implémenter et je pense que ce que j'avais écris en 2015 s'applique toujours. [Voir l'article](/2015/10/09/comment-integrer-les-commentaires-isso-sur-ghost/)

Ghost comme Hugo tente d'imposer l'utilisation de Disqus mais mettre un service extérieur sur mon blog alors que je prône le respect de la vie privée me paraissait illogique.

## Comment suivre le visiteur ?

Je me demandais comment je pourrais me faire une idée du nombre de visiteurs de mon blog sur un site statique qui est hébergé sur le GitLab de Framasoft ? Et en fait, j'ai cet information, car grâce (ou à cause) d'Isso, car le script est chargé à chaque ouverture d'une page de type billet afin de récupérer les commentaires. J'obtiens donc grâce au log Nginx, le détails de mes visiteurs. 

Ce que peut donc très bien faire Disqus ou n'importe qu'elle autre serveurs tiers que l'on utilise à tord et à travers (CDN, Fonts Google ...). D'ailleurs à ce sujet, je recommande l'excellent article et travail qu'à fait David Revoy sur son site Pepper&Carrot : [My fight against CDN libraries](https://www.peppercarrot.com/en/article390/my-fight-against-cdn-libraries)

## Attention peinture fraîche !

Bon effectivement, il y a encore quelques petits détails à régler sur mon installation. Notamment au niveau du thème où je n'ai pas encore mis tout ce qui est réseau social, je me retrouve avec des choses en doublon, j'ai les billets liés qui sont cachés tout en bas ... 

Bref, encore un peu de travail :)
