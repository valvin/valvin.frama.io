+++
author = "ValVin"
categories = ["linux", "android"]
date = 2016-11-15T19:21:14Z
description = ""
draft = false
featured_image = "/content/images/2016/11/jq-1.png"
slug = "utiliser-jq-pour-parser-le-json-dune-api-rest"
tags = ["linux", "android"]
title = "Utiliser jq pour parser le json d'une API REST"

+++

Dans mon travail de tous les jours, je travaille sur des MDM (Mobile Device Managment) qui, selon l'éditeur, fournissent des APIs REST. 
C'est le cas par exemple pour **Airwatch**.

Cet après-midi, je devais trouver le nombre de terminaux sur lesquels une application particulière était installée.  J'ai essayé de trouver le résultat avec une commande `curl` et [**`jq`**](https://stedolan.github.io/jq/)

J'avoue que finalement je n'ai pas réussi en ligne de commande et que j'ai opté pour un script nodejs mais j'ai tout de même appris des choses.

Le webservice fournit un résultat comme celui-ci :

```json
"DeviceApps": [
    {
      "ApplicationName": "Adobe Reader",
      "Version": "10.3.1",
      "BuildVersion": "",
      "Status": 2,
      "Size": "0",
      "ApplicationIdentifier": "com.adobe.reader",
      "Type": "System",
      "IsManaged": false
    },
    {
      "ApplicationName": "AirWatch Agent",
      "Version": "4.3.6.595",
      "BuildVersion": "",
      "Status": 2,
      "Size": "0",
      "ApplicationIdentifier": "com.airwatch.androidagent",
      "Type": "Public",
      "IsManaged": false
    },
    {
      "ApplicationName": "Navigateur",
      "Version": "4.2.1-54",
      "BuildVersion": "",
      "Status": 2,
      "Size": "0",
      "ApplicationIdentifier": "com.android.browser",
      "Type": "System",
      "IsManaged": false
    },
    {
      "ApplicationName": "Calculatrice",
      "Version": "4.2.1-54",
      "BuildVersion": "",
      "Status": 2,
      "Size": "0",
      "ApplicationIdentifier": "com.android.calculator2",
      "Type": "System",
      "IsManaged": false
    },
    {
      "ApplicationName": "Chrome",
      "Version": "28.0.1500.94",
      "BuildVersion": "",
      "Status": 2,
      "Size": "0",
      "ApplicationIdentifier": "com.android.chrome",
      "Type": "System",
      "IsManaged": false
    },
    ...
  ]
}
```

Pour faire la requête, il fallait donc filtrer sur le nom du package `ApplicationIdentifier` et sur l'état `Status` (j'imagine que plein d'autre moyen plus simple existe avec sed / awk et cie)

Ma ligne de commande ressemble donc à cela :

```bash
curl -u user:pass -H Content-Type:application/json -H aw-tenant-code:key https://mdm.domain.tld/API/v1/mdm/devices/675/apps |jq '.DeviceApps[]| select(.ApplicationIdentifier == "com.xxxx.yyyy")| select(.Status == 2)|{ApplicationIdentifier,Version}'
```

* `.DeviceApps[]` : permet de sélectionner le tableau d'objet du même nom
* `select(.ApplicationIdentifier == "com.xxxx.yyyy")` : permet de filtrer sur une propriété de l'objet se trouvant dans le tableau. (idem pour `Status`)
* `{ApplicationIdentifier,Version}` : permet d'afficher à la console un objet disposant des propriétés `ApplicationIdentifier` et `Version` des objets correspondants aux critères précédents

Par exemple pour Calculatrice, cela donnerait : 

```json
{
  "ApplicationIdentifier": "com.android.calculator2",
  "Version": "4.2.1-54"
}
```

Vu le temps passé à comprendre, je me suis dit que ça vallait le coup de partager.
