+++
author = "ValVin"
categories = ["framasoft", "opensource"]
date = 2016-09-17T17:45:28Z
description = ""
draft = false
featured_image = "/content/images/2016/09/pier-349672_1280.jpg"
slug = "mattermost-et-un-channel-irc-cest-possible"
tags = ["framasoft", "opensource"]
title = "Mattermost et un channel IRC c'est possible"

+++

## Mattermost avec Framateam pour Pepper&Carrot

[Framasoft](https://framasoft.org) m'a fait découvrir [**Mattermost**](https://www.mattermost.org/) au travers de leur service [Framateam](https://framateam.org).

Mattermost est un Slack-like, open-source et qui peut s'autohéberger. Cela n'engage que moi, je le décrirais comme un **IRC 2.0**. On consitue une *team* qui peut être public ou privée et dans cette *team* on a la possibilité de créer des *channels* ou tous les membres de l'équipes peuvent échanger. 
Le côté IRC 2.0 sont les suivants :

* format de texte Markdown ce qui permet d'enrichir le texte
* prévisualisation des médias (image, vidéo, son...)
* historique des conversations / mode hors ligne
* mode conversation (lien entre les messages)

![](/content/images/2016/09/framateam.png)

J'ai récemment découvert la BD open-source [**Pepper&Carrot**](http://www.peppercarrot.com) qui a fait un bruit en raison de la sortie d'une version imprimée par l'éditeur *Glénat*. J'en parle dans mon [billet précédent](https://blog.valvin.fr/2016/09/06/pepper-et-carrot-une-bd-sous-licence-libre/)

![](/content/images/2016/09/2016-06-05_pepper-and-carrot_happy-corner-screen_by-David-Revoy.jpg)

Pour dialoguer avec la communauté autour de la BD, il faut aller sur le channel [#pepper&carrot](http://webchat.freenode.net/?channels=%23pepper%26carrot) sur freenode.

![](/content/images/2016/09/irc-peppercarrot.png)

J'ai proposé à l'auteur, David Revoy, de mettre en place [Framateam](https://framateam.org/peppercarrot) pour justement bénéficier des avantages de Mattermost. Comme la communauté est déjà sur Freenode, on a convenu de tenter d'utiliser Mattermost comme canal francophone et voir ce que ça donnait (ça se passe [ici](https://framateam.org/signup_user_complete/?id=gcaq67sntfgr5jbmoaogsgdfqc)). Cependant faire une passerelle vers IRC semblait être une évidence mais comment faire ?

## Matterbridge

C'est la qu'intervient le projet [**Matterbridge**](https://github.com/42wim/matterbridge) qui dans sa version 0.6.1 permet de relier Mattermost avec :

* IRC
* XMPP
* Gitter
* Slack

Dans mon cas, j'utilise seulement la passerelle IRC et plutôt qu'utiliser la version webhook, il existe une version *plus* qui permet d'utiliser les dernières API Mattermost (3.3.0).

![](/content/images/2016/09/framateam-matterbridge-peppercarrot.png)

#### Le binaire
Il suffit donc de compiler, ou réccupérer le binaire de la version 0.6.1 : [ici](https://github.com/42wim/matterbridge/releases/) 

Pour ma part, je ne suis pas certains que ce soit dans les règles de l'art mais je l'ai mis dans `/opt/matterbridge` avec son fichier de configuration dont je parle ci-dessous.

Pour le faire tourner avec *systemd*, j'ai repris un de mes services qui me sert à faire tourner du nodejs :
```
[Service]
ExecStart=/opt/matterbridge/matterbridge-linux64 -plus -conf /opt/matterbridge/matterbridge.conf
Restart=always
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=matterbridge
User=matterbridge
Group=matterbridge

[Install]
WantedBy=multi-user.target
```
L'option `-plus` me permet d'utiliser les dernières API Mattermost, `-conf` permet de spécifier l'emplacement du fichier.

### La configuration - Matterbridge.conf
Il faut le paramétrer avec le fichier matterbridge.conf à partir du template : [ici](https://github.com/42wim/matterbridge/blob/master/matterbridge.conf.sample) qui contient quelque petites coquilles facilement corrigeable. Ca se passe au niveau des paramètres `RemoteNickFormat` dont les `"` ne sont pas fermées.

Voici donc les parties importantes de la configuration. Avant cela, même si c'est évident, il faut pour chaque section activer son fonctionnement avec `Enable=true`

#### Section IRC
Tout d'abord le serveur ou se trouve le channel que l'on veut lier à notre team. Ici freenode. Etonément le channel se paramètre ailleurs.

J'avoue que c'est pas beau, je n'ai pas encore basculé en TLS/SSL.

```
Server="irc.freenode.net:6667"
```

Le *Nick* utilisé par notre passerelle et son identification si nécessaire.
``` 
Nick="matterbot"
NickServNick="nickserv"
NickServPassword="secret"
```

La façon dont est retranscrit un membre de la team sur le channel IRC :
```

RemoteNickFormat="<{NICK}>: "
```
Je me suis aligné sur le format déjà utilisé par une passerelle Telegram. Si j'envoie un message depuis Mattermost, il sera préfixé par `<valvin>: ` 

#### Section Mattermost en mode *plus*
Le serveur qui héberge l'instance Mattermost

```
Server="framateam.org"
```

La team correspondante
```
Team="peppercarrot"
``` 

Le membre de la team utilisé pour la passerelle :

```
Login="mattermostUser"
Password="mattermostPasswd"
```
Pour avoir le nick d'un message en provenance d'IRC il faut activer ce paramètre qui par défaut est à `false`

```
PrefixMessagesWithNick=true
```
Et pour le formattage, tout comme pour IRC on a ce paramètre :
```
RemoteNickFormat="**{NICK}** :"
```
qui aura pour conséquence d'afficher le nick de l'utilisateur IRC en préfixe devant ces messages dans Mattermost comme ceci => **valvin** :

#### Section Channel

Ensuite, il faut relier le channel IRC à celui de Mattermost. Dans mon cas, le canal IRC Freenode est *#pepper&carrot* et le canal Mattermost est *irc*. 
**Attention**, pour le canal Mattermost, c'est sensible à la casse et il faut bien reprendre l'identifiant du canal.

```
IRC="#pepper&carrot"
mattermost="irc"
```

#### Section General
Pour activer le mode *plus*, même si ça fait doublon avec le paramètre de l'éxecutable :
```
Plus=true
```



*Crédit photos : [Pixabay - CC0](https://pixabay.com/en/pier-harbor-walkway-sunset-seaside-349672/) - [David Revoy - CC-BY](http://www.peppercarrot.com/0_sources/0ther/misc/low-res/2016-06-05_pepper-and-carrot_happy-corner-screen_by-David-Revoy.jpg)*
