#!/bin/bash
for i in $(grep -r featured_image content | grep -v "en.md"|sed -e 's@.*featured_image \?\(=\|:\) \?"/\(.*\)"@\2@');
do
    filename=$(basename $i)
    dir=$(dirname $i)
    thumb="$dir/thumb_$filename"
    convert static/$i -resize 450x public/$thumb
    sed -e "s@$filename@thumb_$filename@" -i public/index.html
done
